/**
 * @source: https://www.npmjs.com/package/@kkitahara/linear-algebra
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @typedef {RealAlgebraicElement|ComplexAlgebraicElement}
 * ScalarAlgebraicElement
 *
 * @desc
 * A ScalarAlgebraicElement denotes a {@link RealAlgebraElement}
 * (see @kkitahara/real-algebra) or a {@link ComplexAlgebraElement}
 * (see @kkitahara/complex-algebra).
 */

/**
 * @desc
 * The Matrix class is a class for matrices and vectors.
 *
 * The elements are considered to be stored in row-major order.
 *
 * @version 1.0.0
 * @since 1.0.0
 */
export class Matrix extends Array {
  /**
   * @desc
   * The constructor function of the {@link Matrix} class.
   *
   * CAUTION: this constructor function does not check if `args`
   * are valid {@link ScalarAlgebraicElement}s.
   *
   * @param {...ScalarAlgebraicElement} args
   * instances of a {@link ScalarAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let a = new M(3)
   * a instanceof M // true
   * a.length === 1 // true
   * a[0] === 3 // true
   * a.nRow === 0 // true
   * a.nCol === 0 // true
   *
   * let b = new M(1, 2, 3)
   * b instanceof M // true
   * b.length === 3 //true
   * b[0] === 1 // true
   * b[1] === 2 // true
   * b[2] === 3 // true
   * b.nRow === 0 // true
   * b.nCol === 0 // true
   */
  constructor (...args) {
    if (args.length === 1) {
      super()
      this.push(args[0])
    } else {
      super(...args)
    }
    /**
     * @desc
     * Matrix#nRow stores the number of the rows.
     * If {@link Matrix#nRow} is 0,
     * the number o the rows is automatically determined.
     * by {@link Matrix#getNRow}.
     *
     * You can use {@link Matrix#setDim} to set this parameter.
     *
     * @type {number}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.nRow = 0 // 0 means auto
    /**
     * @desc
     * Matrix#nCol stores the number of the columns.
     * If {@link Matrix#nCol} is 0,
     * the number o the columns is automatically determined
     * by {@link Matrix#getNCol}.
     *
     * You can use {@link Matrix#setDim} to set this parameter.
     *
     * @type {number}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.nCol = 0 // 0 means auto
    /**
     * @desc
     * Matrix#permutation is used if `this` is LU-factorised.
     *
     * @type {Array}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.permutation = null
  }

  /**
   * @desc
   * Thanks to this species, built-in {@link Array} methods,
   * such as the {@link Matrix}#slice method,
   * return an {@link Array} object.
   *
   * @type {function}
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let a = new M(1, 2, 3, 4, 5)
   * let b = a.slice()
   *
   * a.length === 5 // true
   * b.length === 5 // true
   * a === b // false
   * a.constructor === M // true
   * b.constructor === M // false
   * b.constructor === Array // true
   */
  static get [Symbol.species] () {
    return Array
  }

  /**
   * @desc
   * The Matrix#setDim method sets
   * the values of {@link Matrix#nRow} and {@link Matrix#nCol}
   * and return `this`.
   *
   * Automatic dimension determination:
   * let `this` be an instance of {@link Matrix},
   * - if both `this.nRow` and `this.nCol` are `0` and `this.length` is not `1`,
   * the dimension of `this` is adaptive, changes depending on context.
   * - if `this.nRow` is not `0` and `this.nCol` is `0`,
   * `this.nCol` is considered to be equal to
   * `Math.round(this.length / this.nRow)`,
   * - and if `this.nCol` is not `0` and `this.nRow` is `0`,
   * `this.nRow` is considered to be equal to
   * `Math.round(this.length / this.nCol)`,
   * - if `this.nRow` and `this.nCol` are `0`, and `this.length` is `1`,
   * `this` is a 1 x 1 matrix.
   *
   * If `nRow * nCol` (after auto-determination) is
   * not equal to the length of the matrix when a method is called,
   * the method throws an Error.
   *
   * The default values are `0` for both {@link Matrix#nRow}
   * and {@link Matrix#nCol}.
   *
   * @param {number} [nRow = 0]
   * a non-negative integer.
   *
   * @param {number} [nCol = 0]
   * a non-negative integer.
   *
   * @return {Matrix}
   * `this`.
   *
   * @throws {Error}
   * if `nRow` or `nCol` is not a non-negative integer.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let a = new M(1, 2, 3, 4).setDim(2, 2)
   * a.nRow === 2 // true
   * a.nCol === 2 // true
   *
   * a.setDim(1, 2)
   * a.nRow === 1 // true
   * a.nCol === 2 // true
   *
   * a.setDim(-2, 2) // Error
   * a.setDim(2, -2) // Error
   * a.setDim(null, 2) // Error
   * a.setDim(2, null) // Error
   * a.setDim(2.1, 2) // Error
   * a.setDim(2, 2.1) // Error
   */
  setDim (nRow = 0, nCol = 0) {
    if (typeof nRow !== 'number' || !Number.isInteger(nRow) || nRow < 0) {
      throw Error('nRow must be a non-negative integer.')
    }
    if (typeof nCol !== 'number' || !Number.isInteger(nCol) || nCol < 0) {
      throw Error('nCol must be a non-negative integer.')
    }
    this.nRow = nRow
    this.nCol = nCol
    return this
  }

  /**
   * @desc
   * The Matrix#isAdaptive method checks if `this` is adaptive.
   * `this` is considered to be adaptive if both `this.nRow`
   * and `this.nCol` are `0`.
   *
   * @return {boolean}
   * `true` if `this` is adaptive and `false` otherwise.
   *
   * @version 2.1.1
   * @since 2.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let a = new M(1, 2, 3, 4)
   *
   * a.isAdaptive() // true
   *
   * a.setDim(1, 4)
   * a.isAdaptive() // false
   *
   * a.setDim(0, 0)
   * a.isAdaptive() // true
   *
   * a.setDim(0, 4)
   * a.isAdaptive() // false
   *
   * a.setDim()
   * a.isAdaptive() // true
   *
   * a = new M()
   * a.isAdaptive() // true
   */
  isAdaptive () {
    return this.nRow === 0 && this.nCol === 0
  }

  /**
   * @desc
   * The Matrix#getDim method returns the dimension
   * of `this` according to the rule of the automatic size determination.
   *
   * @param {Number} [nRow = 0]
   * used instead of `this.nRow` if `this` is adaptive.
   *
   * @param {Number} [nCol = 0]
   * used instead of `this.nCol` if `this` is adaptive.
   *
   * @return {Array}
   * an array containg the dimension of `this` like `[nRow, nCol]`.
   *
   * @throws {Error}
   * if `this` is considered to be a vector.
   *
   * @throws {Error}
   * if `nRow * nCol` is not equal to `this.length`.
   *
   * @version 2.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let a = new M(1, 2, 3, 4)
   *
   * a.setDim(1, 4)
   * let dim = a.getDim()
   * dim[0] // 1
   * dim[1] // 4
   *
   * a.setDim(2, 2)
   * dim = a.getDim()
   * dim[0] // 2
   * dim[1] // 2
   *
   * a.setDim(0, 2)
   * dim = a.getDim()
   * dim[0] // 2
   * dim[1] // 2
   *
   * a.setDim(0, 4)
   * dim = a.getDim()
   * dim[0] // 1
   * dim[1] // 4
   *
   * a.setDim(1, 3)
   * a.getDim() // Error
   *
   * a.setDim(0, 5)
   * a.getDim() // Error
   *
   * a.setDim(3, 0)
   * a.getDim() // Error
   *
   * a = new M()
   * dim = a.getDim()
   * dim[0] // 0
   * dim[1] // 0
   */
  getDim (nRow = 0, nCol = 0) {
    if (!this.isAdaptive()) {
      nRow = this.nRow
      nCol = this.nCol
    }
    if (nRow === 0) {
      if (nCol === 0) {
        if (this.length === 0) {
          return [0, 0]
        } else {
          throw Error('dimension of `this` is unknown.')
        }
      } else {
        if (this.length % nCol !== 0) {
          throw Error('nRow * nCol and length must be the same.')
        }
        return [this.length / nCol, nCol]
      }
    } else if (nCol === 0) {
      if (this.length % nRow !== 0) {
        throw Error('nRow * nCol and length must be the same.')
      }
      return [nRow, this.length / nRow]
    } else {
      if (nRow * nCol === this.length) {
        return [nRow, nCol]
      } else {
        throw Error('nRow * nCol and length must be the same.')
      }
    }
  }

  /**
   * @desc
   * The Matrix#hasSameDim method checks
   * if the dimension of `this` is the same as that of 'another'.
   *
   * @param {Matrix} another
   * a {@link Matrix}.
   *
   * @return {boolean}
   * `true` if the dimension of `this` is the same as that of `another`
   * and `false` otherwise.
   *
   * @throw {Error}
   * if `another` is not a {@link Matrix}.
   *
   * @version 2.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let m1 = new M(1, 2, 3, 4)
   * let m2 = new M(1, 2, 3, 4).setDim(1, 4)
   * let m3 = new M(1, 2, 3)
   * m1.hasSameDim(m2) // true
   *
   * m2.setDim(4, 1)
   * m1.hasSameDim(m2) // true
   *
   * m2.setDim(2, 2)
   * m1.hasSameDim(m2) // true
   *
   * m1.setDim(2, 2)
   * m1.hasSameDim(m2) // true
   *
   * m1.setDim(1, 4)
   * m1.hasSameDim(m2) // false
   *
   * m1.hasSameDim(m3) // false
   *
   * m1.hasSameDim(null) // Error
   */
  hasSameDim (another) {
    if (!(another instanceof Matrix)) {
      throw Error('`another` is not a `Matrix`.')
    }
    if (this.length === another.length) {
      if (this.isAdaptive() || another.isAdaptive()) {
        return true
      } else {
        const dima = this.getDim()
        const dimb = another.getDim()
        return dima[0] === dimb[0] && dima[1] === dimb[1]
      }
    } else {
      return false
    }
  }

  /**
   * @desc
   * The Matrix#swapRow method swaps
   * *i*-th row and *j*-th row of `this` *in place*.
   *
   * @param {number} i
   * the index of a row to be swapped.
   *
   * @param {number} j
   * the index of the other row to be swapped.
   *
   * @return {Matrix}
   * `this`.
   *
   * @throw {Error}
   * if `i` or `j` is out of range.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let m1 = new M(1, 2, 3, 4).setDim(2, 2)
   * m1.swapRow(0, 1)
   *
   * m1[0] === 3 // true
   * m1[1] === 4 // true
   * m1[2] === 1 // true
   * m1[3] === 2 // true
   *
   * m1.swapRow(0, 0)
   * m1[0] === 3 // true
   * m1[1] === 4 // true
   * m1[2] === 1 // true
   * m1[3] === 2 // true
   *
   * m1.swapRow(0, -1) // Error
   * m1.swapRow(-1, 0) // Error
   */
  swapRow (i, j) {
    const dim = this.getDim()
    const nRow = dim[0]
    if (i < 0 || nRow <= i) {
      throw Error('must be 0 <= i <= nRow.')
    }
    if (j < 0 || nRow <= j) {
      throw Error('must be 0 <= j <= nRow.')
    }
    if (i === j) {
      return this
    } else {
      const nCol = dim[1]
      const ni = nCol * i
      const nj = nCol * j
      for (let k = nCol - 1; k >= 0; k -= 1) {
        const nik = ni + k
        const njk = nj + k
        const swap = this[nik]
        this[nik] = this[njk]
        this[njk] = swap
      }
      return this
    }
  }

  /**
   * @desc
   * The Matrix#swapCol method swaps
   * *i*-th column and *j*-th column of `this` *in place*.
   *
   * @param {number} i
   * the index of a column to be swapped.
   *
   * @param {number} j
   * the index of the other column to be swapped.
   *
   * @return {Matrix}
   * `this`.
   *
   * @throw {Error}
   * if `i` or `j` is out of range.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let m1 = new M(1, 2, 3, 4).setDim(2, 2)
   * m1.swapCol(0, 1)
   *
   * m1[0] === 2 // true
   * m1[1] === 1 // true
   * m1[2] === 4 // true
   * m1[3] === 3 // true
   *
   * m1.swapCol(0, 0)
   * m1[0] === 2 // true
   * m1[1] === 1 // true
   * m1[2] === 4 // true
   * m1[3] === 3 // true
   *
   * m1.swapCol(0, -1) // Error
   * m1.swapCol(-1, 0) // Error
   */
  swapCol (i, j) {
    const dim = this.getDim()
    const nCol = dim[1]
    if (i < 0 || nCol <= i) {
      throw Error('must be 0 <= i <= nCol.')
    }
    if (j < 0 || nCol <= j) {
      throw Error('must be 0 <= j <= nCol.')
    }
    if (i === j) {
      return this
    } else {
      const nRow = dim[0]
      for (let k = nRow - 1; k >= 0; k -= 1) {
        const nk = nCol * k
        const nki = nk + i
        const nkj = nk + j
        const swap = this[nki]
        this[nki] = this[nkj]
        this[nkj] = swap
      }
      return this
    }
  }

  /**
   * @desc
   * The Matrix#isSquare method checks if `this` is a square matrix.
   *
   * @return {boolean}
   * `true` if `this` is a square matrix and `false` otherwise.
   *
   * @throws {Error}
   * if `this` is not considered to be a matrix.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let a = new M(1, 2, 3, 4)
   * a.isSquare() // Error
   *
   * a.setDim(1, 4)
   * a.isSquare() // false
   *
   * a.setDim(2, 2)
   * a.isSquare() // true
   */
  isSquare () {
    const dim = this.getDim()
    return dim[0] === dim[1]
  }

  /**
   * @desc
   * The Matrix#isLUP method checks if
   * `this` is an LU-factorised matrix (with partial pivoting).
   * It checks if `this.permutation` is `null` or not.
   *
   * @return {boolean}
   * `true` if `this` is an LU-factorised matrix and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   */
  isLUP () {
    return this.permutation !== null
  }

  /**
   * @desc
   * The Matrix#toString method converts
   * `this` to a human-readable {@link string}.
   *
   * @param {number} [radix]
   * the base to use for representing numeric values.
   *
   * @return {string}
   * a human-readable {@link string} representation of `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let a = new M(1, 2, 3, 4, 5, 6)
   * a.toString() // '(1, 2, 3, 4, 5, 6)'
   *
   * a.setDim(0, 2)
   * a.toString() // '(1, 2,\n 3, 4,\n 5, 6)'
   *
   * let b = new M(1, 5)
   * b.toString(2) // '(1, 101)'
   *
   * let c = new M()
   * c.toString() // '()'
   */
  toString (radix) {
    let s = ''
    const nCol = this.isAdaptive() ? this.length : this.getDim()[1]
    s += '('
    if (this.length >= 1) {
      s += this[0].toString(radix)
    }
    for (let i = 1, n = this.length; i < n; i += 1) {
      if (i % nCol === 0) {
        s += ',\n '
      } else {
        s += ', '
      }
      s += this[i].toString(radix)
    }
    s += ')'
    return s
  }

  /**
   * @desc
   * The Matrix#toFixed method returns the fixed-point representation of `this`.
   *
   * @param {number} [digits]
   * the number of digits appear after the decimal point.
   *
   * @return {string}
   * the fixed-point representation of `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let a = new M(1, 2, 3, 4, 5, 6)
   * a.toFixed(3) // '(1.000, 2.000, 3.000, 4.000, 5.000, 6.000)'
   *
   * a.setDim(0, 2)
   * a.toFixed(3) // '(1.000, 2.000,\n 3.000, 4.000,\n 5.000, 6.000)'
   *
   * let b = new M()
   * b.toFixed(3) // '()'
   */
  toFixed (digits) {
    let s = ''
    const nCol = this.isAdaptive() ? this.length : this.getDim()[1]
    s += '('
    if (this.length >= 1) {
      s += this[0].toFixed(digits)
    }
    for (let i = 1, n = this.length; i < n; i += 1) {
      if (i % nCol === 0) {
        s += ',\n '
      } else {
        s += ', '
      }
      s += this[i].toFixed(digits)
    }
    s += ')'
    return s
  }

  /**
   * @desc
   * The Matrix#toJSON method converts
   * `this` to an object serialisable by `JSON.stringify`.
   *
   * @return {object}
   * a serialisable object for `this`.
   *
   * @version 2.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let a = new M(1, 2, 3, 4, 5, 6).setDim(0, 3)
   *
   * // toJSON method is called by JSON.stringify
   * let s = JSON.stringify(a)
   *
   * typeof s // 'string'
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let a = new M(1, 2, 3, 4, 5, 6).setDim(0, 3)
   * a.permutation = [1, 2, 3, 4]
   *
   * // toJSON method is called by JSON.stringify
   * let s = JSON.stringify(a)
   *
   * typeof s // 'string'
   */
  toJSON () {
    const obj = {}
    obj.reviver = 'Matrix'
    obj.version = '2.0.0'
    obj.data = this.slice()
    obj.nRow = this.nRow
    obj.nCol = this.nCol
    if (this.isLUP()) {
      obj.permutation = this.permutation
    }
    return obj
  }

  /**
   * @desc
   * The Matrix.reviver function converts
   * the data produced by {@link Matrix#toJSON}
   * to a {@link Matrix}.
   *
   * @param {object} key
   *
   * @param {object} value
   *
   * @return {Matrix}
   * a {@link Matrix} or `value`.
   *
   * @throws {Error}
   * if the given object is invalid.
   *
   * @version 2.0.0
   * @since 1.0.0
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let a = new M(1, 2, 3, 4)
   * let s = JSON.stringify(a)
   *
   * // reviver method is called by JSON.parse
   * let b = JSON.parse(s, M.reviver)
   *
   * typeof s // 'string'
   * a === b // false
   * a.length === b.length // true
   * a.nRow === b.nRow // true
   * a.nCol === b.nRow // true
   * a[0] === b[0] // true
   * a[1] === b[1] // true
   * a[2] === b[2] // true
   * a[3] === b[3] // true
   *
   * let s2 = s.replace('2.0.0', '1.0.0')
   * b = JSON.parse(s2, M.reviver)
   * typeof s2 // 'string'
   * a === b // false
   * a.length === b.length // true
   * a.nRow === b.nRow // true
   * a.nCol === b.nRow // true
   * a[0] === b[0] // true
   * a[1] === b[1] // true
   * a[2] === b[2] // true
   * a[3] === b[3] // true
   *
   * let s3 = s.replace('2.0.0', '0.0.0')
   * JSON.parse(s3, M.reviver) // Error
   *
   * @example
   * import { Matrix as M } from '@kkitahara/linear-algebra'
   *
   * let a = new M(1, 2, 3, 4)
   * a.permutation = [1, 2, 3, 4]
   * let s = JSON.stringify(a)
   *
   * // reviver method is called by JSON.parse
   * let b = JSON.parse(s, M.reviver)
   *
   * typeof s // 'string'
   * a === b // false
   * a.length === b.length // true
   * a.nRow === b.nRow // true
   * a.nCol === b.nRow // true
   * a[0] === b[0] // true
   * a[1] === b[1] // true
   * a[2] === b[2] // true
   * a[3] === b[3] // true
   * b.permutation[0] === 1 // true
   * b.permutation[1] === 2 // true
   * b.permutation[2] === 3 // true
   * b.permutation[3] === 4 // true
   */
  static reviver (key, value) {
    if (value !== null && typeof value === 'object' &&
        value.reviver === 'Matrix') {
      switch (value.version) {
        case '1.0.0':
        case '2.0.0':
          const data = value.data
          const nRow = value.nRow
          const nCol = value.nCol
          const m = new Matrix(...data).setDim(nRow, nCol)
          if (value.permutation !== undefined) {
            m.permutation = value.permutation
          }
          return m
        default:
          throw Error('invalid version.')
      }
    } else {
      return value
    }
  }
}

/* @license-end */
