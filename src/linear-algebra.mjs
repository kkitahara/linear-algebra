/**
 * @source: https://www.npmjs.com/package/@kkitahara/linear-algebra
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Matrix } from './matrix.mjs'

/**
 * @typedef {RealAlgebra|ComplexAlgebra} ScalarAlgebra
 *
 * @desc
 * A ScalarAlgebra denotes a {@link RealAlgebra} (see @kkitahara/real-algebra)
 * or a {@link ComplexAlgebra} (see @kkitahara/complex-algebra).
 */

/**
 * @desc
 * The LinearAlgebra class is a class for linear algebra.
 *
 * @version 2.0.0
 * @since 1.0.0
 *
 * @example
 * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
 * // import { RealAlgebra } from '@kkitahara/real-algebra'
 * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
 * import { LinearAlgebra } from '@kkitahara/linear-algebra'
 * let r = new RealAlgebra()
 * let c = new ComplexAlgebra(r)
 * let l = new LinearAlgebra(c)
 * let m1, m2, m3
 *
 * // Generate a new matrix (since v2.0.0)
 * m1 = l.$(1, 0, 0, 1)
 * m1.toString() // '(1, 0, 0, 1)'
 *
 * m1 = l.$(r.$(1, 2, 5), c.$(0, 1), c.$(0, -1), 1)
 * m1.toString() // '((1 / 2)sqrt(5), i(1), i(-1), 1)'
 *
 * // Some Array methods can be used
 * m1 = l.$(r.$(1, 2, 5), c.$(0, 1), c.$(0, -1), 1)
 * m1.push(c.$(3))
 * m1.toString() // '((1 / 2)sqrt(5), i(1), i(-1), 1, 3)'
 *
 * // Set and get the dimension
 * m1 = l.$(1, 0, 0, 1)
 *
 * // 2 x 2 matix
 * m1.setDim(2, 2)
 * // number of rows
 * m1.getDim()[0] // 2
 * // number of columns
 * m1.getDim()[1] // 2
 * // elements are stored in row-major order
 * m1.toString() // '(1, 0,\n 0, 1)'
 *
 * // 1 x 4 matix
 * m1.setDim(1, 4)
 * m1.getDim()[0] // 1
 * m1.getDim()[1] // 4
 * m1.toString() // '(1, 0, 0, 1)'
 *
 * // 4 x 1 matix
 * m1.setDim(4, 1)
 * m1.getDim()[0] // 4
 * m1.getDim()[1] // 1
 * m1.toString() // '(1,\n 0,\n 0,\n 1)'
 *
 * // 3 x 1 matix (inconsistent dimension)
 * m1.setDim(3, 1)
 * // throws an Error when getDim is called
 * m1.getDim() // Error
 *
 * // 2 x 0 (0 means auto)
 * m1.setDim(2, 0)
 * m1.getDim()[0] // 2
 * m1.getDim()[1] // 2
 *
 * // 0 x 1 (0 means auto)
 * m1.setDim(0, 1)
 * m1.getDim()[0] // 4
 * m1.getDim()[1] // 1
 *
 * // 0 x 0 (this is an adaptive matrix, since v2.0.0)
 * m1.setDim(0, 0)
 * m1.isAdaptive() // true
 * m1.getDim() // Error
 *
 * // matrices are adaptive by default
 * m1 = l.$(1, 0, 0, 1)
 * m1.isAdaptive() // true
 *
 * // Copy (generate a new object)
 * m1 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m2 = l.copy(m1)
 * m2.toString() // '(1, 0,\n 0, 1)'
 * m2.getDim()[0] // 2
 * m2.getDim()[1] // 2
 *
 * // Equality
 * m1 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m2 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m3 = l.$(1, 0, 0, -1).setDim(2, 2)
 * l.eq(m1, m2) // true
 * l.eq(m1, m3) // false
 *
 * // matrices of different dimension are considered to be not equal
 * m1 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m2 = l.$(1, 0, 0, 1).setDim(1, 4)
 * l.eq(m1, m2) // false
 *
 * // here, `m1` is adaptive
 * m1 = l.$(1, 0, 0, 1)
 * m2 = l.$(1, 0, 0, 1).setDim(1, 4)
 * m3 = l.$(1, 0, 0, 1).setDim(4, 1)
 * l.eq(m1, m2) // true
 * l.eq(m1, m3) // true
 * l.eq(m2, m3) // false
 *
 * // Inequality
 * m1 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m2 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m3 = l.$(1, 0, 0, -1).setDim(2, 2)
 * l.ne(m1, m2) // false
 * l.ne(m1, m3) // true
 *
 * // matrices of different dimension are considered to be not equal
 * m1 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m2 = l.$(1, 0, 0, 1).setDim(1, 4)
 * l.ne(m1, m2) // true
 *
 * // here, `m1` is adaptive
 * m1 = l.$(1, 0, 0, 1)
 * m2 = l.$(1, 0, 0, 1).setDim(1, 4)
 * m3 = l.$(1, 0, 0, 1).setDim(4, 1)
 * l.ne(m1, m2) // false
 * l.ne(m1, m3) // false
 * l.ne(m2, m3) // true
 *
 * // isZero
 * m1 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m2 = l.$(0, 0, 0, 0).setDim(2, 2)
 * l.isZero(m1) // false
 * l.isZero(m2) // true
 *
 * // isInteger (since v1.1.0)
 * m1 = l.$(1, r.$(4, 2), -3, 4).setDim(2, 2)
 * m2 = l.$(1, r.$(1, 2), -3, 4).setDim(2, 2)
 * l.isInteger(m1) // true
 * l.isInteger(m2) // false
 *
 * // Element-wise addition
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is generated
 * m3 = l.add(m1, m2)
 * m3.toString() // '(2, 5, 4, 7)'
 *
 * // In-place element-wise addition
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is not generated
 * m1 = l.iadd(m1, m2)
 * m1.toString() // '(2, 5, 4, 7)'
 *
 * // Element-wise subtraction
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is generated
 * m3 = l.sub(m1, m2)
 * m3.toString() // '(0, -1, 2, 1)'
 *
 * // In-place element-wise subtraction
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is not generated
 * m1 = l.isub(m1, m2)
 * m1.toString() // '(0, -1, 2, 1)'
 *
 * // Element-wise multiplication
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is generated
 * m3 = l.mul(m1, m2)
 * m3.toString() // '(1, 6, 3, 12)'
 *
 * // In-place element-wise multiplication
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is not generated
 * m1 = l.imul(m1, m2)
 * m1.toString() // '(1, 6, 3, 12)'
 *
 * // Element-wise division
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is generated
 * m3 = l.div(m1, m2)
 * m3.toString() // '(1, 2 / 3, 3, 4 / 3)'
 *
 * // In-place element-wise division
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is not generated
 * m1 = l.idiv(m1, m2)
 * m1.toString() // '(1, 2 / 3, 3, 4 / 3)'
 *
 * // Scalar multiplication
 * m1 = l.$(1, 2, 3, 4)
 * // new object is generated
 * m2 = l.smul(m1, r.$(1, 2))
 * m2.toString() // '(1 / 2, 1, 3 / 2, 2)'
 *
 * // In-place scalar multiplication
 * m1 = l.$(1, 2, 3, 4)
 * // new object is not generated
 * m1 = l.ismul(m1, r.$(1, 2))
 * m1.toString() // '(1 / 2, 1, 3 / 2, 2)'
 *
 * // Scalar multiplication by -1
 * m1 = l.$(1, 2, 3, 4)
 * // new object is generated
 * m2 = l.neg(m1)
 * m2.toString() // '(-1, -2, -3, -4)'
 *
 * // In-place scalar multiplication by -1
 * m1 = l.$(1, 2, 3, 4)
 * // new object is not generated
 * m1 = l.ineg(m1)
 * m1.toString() // '(-1, -2, -3, -4)'
 *
 * // Scalar division (since v2.0.0)
 * m1 = l.$(1, 2, 3, 4)
 * // new object is generated
 * m2 = l.sdiv(m1, 2)
 * m2.toString() // '(1 / 2, 1, 3 / 2, 2)'
 *
 * // In-place scalar division (since v2.0.0)
 * m1 = l.$(1, 2, 3, 4)
 * // new object is not generated
 * m1 = l.isdiv(m1, 2)
 * m1.toString() // '(1 / 2, 1, 3 / 2, 2)'
 *
 * // Complex conjugate
 * m1 = l.$(1, c.$(0, 2), 3, c.$(0, 4))
 * // new object is generated
 * m2 = l.cjg(m1)
 * m2.toString() // '(1, i(-2), 3, i(-4))'
 *
 * // In-place evaluation of the complex conjugate
 * m1 = l.$(1, c.$(0, 2), 3, c.$(0, 4))
 * // new object is not generated
 * m1 = l.icjg(m1)
 * m1.toString() // '(1, i(-2), 3, i(-4))'
 *
 * // Transpose
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * // new object is generated
 * m2 = l.transpose(m1)
 * m2.toString() // '(1, 3,\n 2, 4)'
 *
 * // In-place evaluation of the transpose
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * // new object is not generated
 * m1 = l.itranspose(m1)
 * m1.toString() // '(1, 3,\n 2, 4)'
 *
 * // Conjugate transpose (Hermitian transpose)
 * m1 = l.$(1, c.$(0, 2), 3, c.$(0, 4)).setDim(2, 2)
 * // new object is generated
 * m2 = l.cjgTranspose(m1)
 * m2.toString() // '(1, 3,\n i(-2), i(-4))'
 *
 * // In-place evaluation of the conjugate transpose
 * m1 = l.$(1, c.$(0, 2), 3, c.$(0, 4)).setDim(2, 2)
 * // new object is not generated
 * m1 = l.icjgTranspose(m1)
 * m1.toString() // '(1, 3,\n i(-2), i(-4))'
 *
 * // Dot product
 * m1 = l.$(1, c.$(0, 2))
 * m2 = l.$(c.$(0, 3), 2)
 * l.dot(m1, m2).toString() // 'i(-1)'
 *
 * // Square of the absolute value (Frobenius norm)
 * m1 = l.$(1, c.$(0, 2))
 * let a = l.abs2(m1)
 * a.toString() // '5'
 * // return value is not a complex number (but a real number)
 * a.re // undefined
 * a.im // undefined
 *
 * // Matrix multiplication
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * m2 = l.$(1, 3, 1, 3).setDim(2, 2)
 * m3 = l.mmul(m1, m2)
 * m3.toString() // '(3, 9,\n 7, 21)'
 *
 * // LU-factorisation
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * // new object is generated
 * m2 = l.lup(m1)
 *
 * // In-place LU-factorisation
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * // new object is not generated
 * m1 = l.ilup(m1)
 *
 * // Solving a linear equation
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * m2 = l.$(1, 2, 3, 4).setDim(2, 2)
 *
 * // m1 m3 = m2, new object is generated
 * m3 = l.solve(l.lup(m1), m2)
 * m3.toString() // '(1, 0,\n 0, 1)'
 *
 * // m3 m1 = m2, new object is generated
 * m3 = l.solve(m2, l.lup(m1))
 * m3.toString() // '(1, 0,\n 0, 1)'
 *
 * // Solving a linear equation in-place
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * m2 = l.$(1, 2, 3, 4).setDim(2, 2)
 *
 * // m1 m3 = m2, new object is not generated
 * m2 = l.isolve(l.lup(m1), m2)
 * m2.toString() // '(1, 0,\n 0, 1)'
 *
 * m2 = l.$(1, 2, 3, 4).setDim(2, 2)
 *
 * // m3 m1 = m2, new object is not generated
 * m2 = l.isolve(m2, l.lup(m1))
 * m2.toString() // '(1, 0,\n 0, 1)'
 *
 * // Determinant
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * m2 = l.lup(m1)
 * // det method supports only LU-factorised matrices
 * let det = l.det(m2)
 * det.toString() // '-2'
 *
 * // JSON (stringify and parse)
 * m1 = l.$(1, r.$(2, 3, 5), 3, c.$(0, r.$(4, 5, 3))).setDim(2, 2)
 * let str = JSON.stringify(m1)
 * m2 = JSON.parse(str, l.reviver)
 * l.eq(m1, m2) // true
 *
 * @example
 * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
 * // import { RealAlgebra } from '@kkitahara/real-algebra'
 * import { LinearAlgebra } from '@kkitahara/linear-algebra'
 * let r = new RealAlgebra()
 * let l = new LinearAlgebra(r)
 * let m1, m2, m3
 *
 * // Generate a new matrix (since v2.0.0)
 * m1 = l.$(1, 0, 0, 1)
 * m1.toString() // '(1, 0, 0, 1)'
 *
 * m1 = l.$(r.$(1, 2, 5), 1, -1, 1)
 * m1.toString() // '((1 / 2)sqrt(5), 1, -1, 1)'
 *
 * // Some Array methods can be used
 * m1 = l.$(r.$(1, 2, 5), 1, -1, 1)
 * m1.push(r.$(3))
 * m1.toString() // '((1 / 2)sqrt(5), 1, -1, 1, 3)'
 *
 * // Set and get the dimension
 * m1 = l.$(1, 0, 0, 1)
 *
 * // 2 x 2 matix
 * m1.setDim(2, 2)
 * // number of rows
 * m1.getDim()[0] // 2
 * // number of columns
 * m1.getDim()[1] // 2
 * // elements are stored in row-major order
 * m1.toString() // '(1, 0,\n 0, 1)'
 *
 * // 1 x 4 matix
 * m1.setDim(1, 4)
 * m1.getDim()[0] // 1
 * m1.getDim()[1] // 4
 * m1.toString() // '(1, 0, 0, 1)'
 *
 * // 4 x 1 matix
 * m1.setDim(4, 1)
 * m1.getDim()[0] // 4
 * m1.getDim()[1] // 1
 * m1.toString() // '(1,\n 0,\n 0,\n 1)'
 *
 * // 3 x 1 matix (inconsistent dimension)
 * m1.setDim(3, 1)
 * // throws an Error when getDim is called
 * m1.getDim() // Error
 *
 * // 2 x 0 (0 means auto)
 * m1.setDim(2, 0)
 * m1.getDim()[0] // 2
 * m1.getDim()[1] // 2
 *
 * // 0 x 1 (0 means auto)
 * m1.setDim(0, 1)
 * m1.getDim()[0] // 4
 * m1.getDim()[1] // 1
 *
 * // 0 x 0 (this is an adaptive matrix, since v2.0.0)
 * m1.setDim(0, 0)
 * m1.isAdaptive() // true
 * m1.getDim() // Error
 *
 * // matrices are adaptive by default
 * m1 = l.$(1, 0, 0, 1)
 * m1.isAdaptive() // true
 *
 * // Copy (generate a new object)
 * m1 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m2 = l.copy(m1)
 * m2.toString() // '(1, 0,\n 0, 1)'
 * m2.getDim()[0] // 2
 * m2.getDim()[1] // 2
 *
 * // Equality
 * m1 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m2 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m3 = l.$(1, 0, 0, -1).setDim(2, 2)
 * l.eq(m1, m2) // true
 * l.eq(m1, m3) // false
 *
 * // matrices of different dimension are considered to be not equal
 * m1 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m2 = l.$(1, 0, 0, 1).setDim(1, 4)
 * l.eq(m1, m2) // false
 *
 * // here, `m1` is adaptive
 * m1 = l.$(1, 0, 0, 1)
 * m2 = l.$(1, 0, 0, 1).setDim(1, 4)
 * m3 = l.$(1, 0, 0, 1).setDim(4, 1)
 * l.eq(m1, m2) // true
 * l.eq(m1, m3) // true
 * l.eq(m2, m3) // false
 *
 * // Inequality
 * m1 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m2 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m3 = l.$(1, 0, 0, -1).setDim(2, 2)
 * l.ne(m1, m2) // false
 * l.ne(m1, m3) // true
 *
 * // matrices of different dimension are considered to be not equal
 * m1 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m2 = l.$(1, 0, 0, 1).setDim(1, 4)
 * l.ne(m1, m2) // true
 *
 * // here, `m1` is adaptive
 * m1 = l.$(1, 0, 0, 1)
 * m2 = l.$(1, 0, 0, 1).setDim(1, 4)
 * m3 = l.$(1, 0, 0, 1).setDim(4, 1)
 * l.ne(m1, m2) // false
 * l.ne(m1, m3) // false
 * l.ne(m2, m3) // true
 *
 * // isZero
 * m1 = l.$(1, 0, 0, 1).setDim(2, 2)
 * m2 = l.$(0, 0, 0, 0).setDim(2, 2)
 * l.isZero(m1) // false
 * l.isZero(m2) // true
 *
 * // Element-wise addition
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is generated
 * m3 = l.add(m1, m2)
 * m3.toString() // '(2, 5, 4, 7)'
 *
 * // In-place element-wise addition
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is not generated
 * m1 = l.iadd(m1, m2)
 * m1.toString() // '(2, 5, 4, 7)'
 *
 * // Element-wise subtraction
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is generated
 * m3 = l.sub(m1, m2)
 * m3.toString() // '(0, -1, 2, 1)'
 *
 * // In-place element-wise subtraction
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is not generated
 * m1 = l.isub(m1, m2)
 * m1.toString() // '(0, -1, 2, 1)'
 *
 * // Element-wise multiplication
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is generated
 * m3 = l.mul(m1, m2)
 * m3.toString() // '(1, 6, 3, 12)'
 *
 * // In-place element-wise multiplication
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is not generated
 * m1 = l.imul(m1, m2)
 * m1.toString() // '(1, 6, 3, 12)'
 *
 * // Element-wise division
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is generated
 * m3 = l.div(m1, m2)
 * m3.toString() // '(1, 2 / 3, 3, 4 / 3)'
 *
 * // In-place element-wise division
 * m1 = l.$(1, 2, 3, 4)
 * m2 = l.$(1, 3, 1, 3)
 * // new object is not generated
 * m1 = l.idiv(m1, m2)
 * m1.toString() // '(1, 2 / 3, 3, 4 / 3)'
 *
 * // Scalar multiplication
 * m1 = l.$(1, 2, 3, 4)
 * // new object is generated
 * m2 = l.smul(m1, r.$(1, 2))
 * m2.toString() // '(1 / 2, 1, 3 / 2, 2)'
 *
 * // In-place scalar multiplication
 * m1 = l.$(1, 2, 3, 4)
 * // new object is not generated
 * m1 = l.ismul(m1, r.$(1, 2))
 * m1.toString() // '(1 / 2, 1, 3 / 2, 2)'
 *
 * // Scalar multiplication by -1
 * m1 = l.$(1, 2, 3, 4)
 * // new object is generated
 * m2 = l.neg(m1)
 * m2.toString() // '(-1, -2, -3, -4)'
 *
 * // In-place scalar multiplication by -1
 * m1 = l.$(1, 2, 3, 4)
 * // new object is not generated
 * m1 = l.ineg(m1)
 * m1.toString() // '(-1, -2, -3, -4)'
 *
 * // Scalar division (since v2.0.0)
 * m1 = l.$(1, 2, 3, 4)
 * // new object is generated
 * m2 = l.sdiv(m1, 2)
 * m2.toString() // '(1 / 2, 1, 3 / 2, 2)'
 *
 * // In-place scalar division (since v2.0.0)
 * m1 = l.$(1, 2, 3, 4)
 * // new object is not generated
 * m1 = l.isdiv(m1, 2)
 * m1.toString() // '(1 / 2, 1, 3 / 2, 2)'
 *
 * // Complex conjugate
 * m1 = l.$(1, 2, 3, 4)
 * // new object is generated
 * m2 = l.cjg(m1)
 * m2.toString() // '(1, 2, 3, 4)'
 *
 * // In-place evaluation of the complex conjugate
 * m1 = l.$(1, 2, 3, 4)
 * // new object is not generated
 * m1 = l.icjg(m1)
 * m1.toString() // '(1, 2, 3, 4)'
 *
 * // Transpose
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * // new object is generated
 * m2 = l.transpose(m1)
 * m2.toString() // '(1, 3,\n 2, 4)'
 *
 * // In-place evaluation of the transpose
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * // new object is not generated
 * m1 = l.itranspose(m1)
 * m1.toString() // '(1, 3,\n 2, 4)'
 *
 * // Conjugate transpose (Hermitian transpose)
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * // new object is generated
 * m2 = l.cjgTranspose(m1)
 * m2.toString() // '(1, 3,\n 2, 4)'
 *
 * // In-place evaluation of the conjugate transpose
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * // new object is not generated
 * m1 = l.icjgTranspose(m1)
 * m1.toString() // '(1, 3,\n 2, 4)'
 *
 * // Dot product
 * m1 = l.$(1, 2)
 * m2 = l.$(3, 2)
 * l.dot(m1, m2).toString() // '7'
 *
 * // Square of the absolute value (Frobenius norm)
 * m1 = l.$(1, 2)
 * let a = l.abs2(m1)
 * a.toString() // '5'
 *
 * // Matrix multiplication
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * m2 = l.$(1, 3, 1, 3).setDim(2, 2)
 * m3 = l.mmul(m1, m2)
 * m3.toString() // '(3, 9,\n 7, 21)'
 *
 * // LU-factorisation
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * // new object is generated
 * m2 = l.lup(m1)
 *
 * // In-place LU-factorisation
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * // new object is not generated
 * m1 = l.ilup(m1)
 *
 * // Solving a linear equation
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * m2 = l.$(1, 2, 3, 4).setDim(2, 2)
 *
 * // m1 m3 = m2, new object is generated
 * m3 = l.solve(l.lup(m1), m2)
 * m3.toString() // '(1, 0,\n 0, 1)'
 *
 * // m3 m1 = m2, new object is generated
 * m3 = l.solve(m2, l.lup(m1))
 * m3.toString() // '(1, 0,\n 0, 1)'
 *
 * // Solving a linear equation in-place
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * m2 = l.$(1, 2, 3, 4).setDim(2, 2)
 *
 * // m1 m3 = m2, new object is not generated
 * m2 = l.solve(l.lup(m1), m2)
 * m2.toString() // '(1, 0,\n 0, 1)'
 *
 * m2 = l.$(1, 2, 3, 4).setDim(2, 2)
 *
 * // m3 m1 = m2, new object is not generated
 * m2 = l.solve(m2, l.lup(m1))
 * m2.toString() // '(1, 0,\n 0, 1)'
 *
 * // Determinant
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * m2 = l.lup(m1)
 * // det method supports only LU-factorised matrices
 * let det = l.det(m2)
 * det.toString() // '-2'
 *
 * // JSON (stringify and parse)
 * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
 * let str = JSON.stringify(m1)
 * m2 = JSON.parse(str, l.reviver)
 * l.eq(m1, m2) // true
 */
export class LinearAlgebra {
  /**
   * @desc
   * The constructor function of the {@link LinearAlgebra} class.
   *
   * CAUTION: this method does not check if `salg` is a valid
   * implementation of {@link ScalarAlgebra} or not.
   *
   * @param {ScalarAlgebra} salg
   * an instance of a {@link ScalarAlgebra}.
   *
   * @version 2.0.2
   * @since 1.0.0
   */
  constructor (salg) {
    /**
     * @desc
     * The LinearAlgebra#salg is used to manipurate elements of {@link Matrix}.
     *
     * @type {ScalarAlgebra}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.salg = salg
  }

  /**
   * @desc
   * The LinearAlgebra#$ method returns a new instance of {@link Matrix}
   * which contains the given parameters copied by `salg.copy` method.
   *
   * @param {...ScalarAlgebraicElement} args
   * instances of a {@link ScalarAlgebraicElement}.
   *
   * @return {Matrix}
   * a new instance of {@link Matrix}.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   */
  $ (...args) {
    const salg = this.salg
    for (let i = args.length - 1; i >= 0; i -= 1) {
      args[i] = salg.copy(args[i])
    }
    return new Matrix(...args)
  }

  /**
   * @desc
   * The LinearAlgebra#cast method returns
   * `m` with its elements casted by `salg.cast` method
   * if `m` is a {@link Matrix}.
   * If `m` is an {@link Array},
   * it returns a new {@link Matrix} containing
   * the elements of the `m` casted by `salg.cast` method;
   * in this case, if `m.nRow`, `m.nCol` or `m.permutation` are set,
   * these are set to the new {@link Matrix}.
   *
   * @param {object} m
   * an instance of {@link Array}.
   *
   * @return {Matrix}
   * `m` or a new {@link Matrix}.
   *
   * @throws {Error}
   * if `m` is not an instance of {@link Array}.
   *
   * @version 2.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * import { ComplexAlgebra, ComplexAlgebraicElement as C }
   *   from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(c.$(1))
   * let m2 = m1
   *
   * m2 === l.cast(m1) // true
   * m1[0] instanceof C // true
   * m1[0].re instanceof P // true
   * r.eq(m1[0].re, 1) // true
   * m1[0].im instanceof P // true
   * r.eq(m1[0].im, 0) // true
   *
   * let m3 = l.cast([1, 2])
   * m3 instanceof M // true
   * m3[0] instanceof C // true
   * m3[0].re instanceof P // true
   * r.eq(m3[0].re, 1) // true
   * m3[0].im instanceof P // true
   * r.eq(m3[0].im, 0) // true
   * m3[1] instanceof C // true
   * m3[1].re instanceof P // true
   * r.eq(m3[1].re, 2) // true
   * m3[1].im instanceof P // true
   * r.eq(m3[1].im, 0) // true
   *
   * let a = r.$(1, 2, 5)
   * a instanceof Array // true
   * l.cast(a) // Error
   *
   * l.cast(2) // Error
   */
  cast (m) {
    const salg = this.salg
    if (m instanceof Matrix) {
      const arr = []
      for (let i = 0, n = m.length; i < n; i += 1) {
        arr.push(salg.cast(m[i]))
      }
      if (arr.some((arri, i) => arri !== m[i])) {
        const nRow = m.nRow
        const nCol = m.nCol
        const permutation = m.permutation
        m = new Matrix(...arr).setDim(nRow, nCol)
        m.permutation = permutation
      }
    } else if (m instanceof Array) {
      const nRow = m.nRow | 0
      const nCol = m.nCol | 0
      const permutation = m.permutation || null
      m = new Matrix(...m).setDim(nRow, nCol)
      m.permutation = permutation
      for (let i = m.length - 1; i >= 0; i -= 1) {
        m[i] = salg.cast(m[i])
      }
    } else {
      throw Error('can cast only instances of `Array`.')
    }
    return m
  }

  /**
   * @desc
   * The LinearAlgebra#copy method returns a copy of `m`.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * a copy of `m`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(1, 2)
   * m1.setDim(2, 0)
   * let m2 = l.copy(m1)
   *
   * m1 instanceof M // true
   * m2 instanceof M // true
   * m1 !== m2 // true
   * m1[0] !== m2[0] // true
   * m1[1] !== m2[1] // true
   * m1.nRow // 2
   * m1.nCol // 0
   * m2.nRow // 2
   * m2.nCol // 0
   * c.eq(m1[0], m2[0]) // true
   * c.eq(m1[1], m2[1]) // true
   *
   * let m3 = l.$(1, 2, 3, 4).setDim(2, 2)
   * m3 = l.ilup(m3)
   * let m4 = l.copy(m3)
   * m4.isLUP() // true
   */
  copy (m) {
    m = this.cast(m)
    const salg = this.salg
    const arr = []
    for (let i = 0, n = m.length; i < n; i += 1) {
      arr.push(salg.copy(m[i]))
    }
    const m2 = new Matrix(...arr).setDim(m.nRow, m.nCol)
    if (m.permutation !== null) {
      m2.permutation = m.permutation.slice()
    }
    return m2
  }

  /**
   * @desc
   * The LinearAlgebra#eq method checks if `m1` is equal to `m2`.
   * Matrices of different dimension are considered to be not equal.
   *
   * @param {Matrix} m1
   * a {@link Matrix}.
   *
   * @param {Matrix} m2
   * a {@link Matrix}.
   *
   * @return {boolean}
   * `true` if `m1` is equal to `m2` and `false` otherwise.
   *
   * @throws {Error}
   * if `m1` or `m2` is LU-factorised.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(1, 2, 3)
   * let m2 = l.$(1, 2, 3)
   * let m3 = l.$(3, 2, 1)
   * let m4 = l.$(1, 2)
   * let m5 = l.$(1, 2, 3, 4)
   *
   * l.eq(m1, m2) // true
   * l.eq(m1, m3) // false
   * l.eq(m1, m4) // false
   * l.eq(m1, m5) // false
   *
   * m1.setDim(1, 3)
   * m2.setDim(3, 1)
   * l.eq(m1, m2) // false
   *
   * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
   * m2 = l.$(1, 2, 3, 4).setDim(2, 2)
   * l.eq(l.lup(m1), m2) // Error
   * l.eq(m1, l.lup(m2)) // Error
   */
  eq (m1, m2) {
    m1 = this.cast(m1)
    m2 = this.cast(m2)
    if (m1.isLUP() || m2.isLUP()) {
      throw Error('LU-factorised matrices are not suppoted by eq method.')
    }
    const salg = this.salg
    if (m1.hasSameDim(m2)) {
      let flag = true
      for (let i = m1.length - 1; flag && i >= 0; i -= 1) {
        flag = salg.eq(m1[i], m2[i])
      }
      return flag
    } else {
      return false
    }
  }

  /**
   * @desc
   * The LinearAlgebra#ne method checks if `m1` is not equal to `m2`.
   *
   * `lalg.eq(m1, m2)` is an alias for `!lalg.eq(m1, m2)`,
   * where `lalg` is an instance of {@link LinearAlgebra}.
   *
   * @param {Matrix} m1
   * a {@link Matrix}.
   *
   * @param {Matrix} m2
   * a {@link Matrix}.
   *
   * @return {boolean}
   * `true` if `m1` is not equal to `m2` and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(1, 2, 3)
   * let m2 = l.$(1, 2, 3)
   * let m3 = l.$(3, 2, 1)
   * let m4 = l.$(1, 2)
   * let m5 = l.$(1, 2, 3, 4)
   *
   * l.ne(m1, m2) // false
   * l.ne(m1, m3) // true
   * l.ne(m1, m4) // true
   * l.ne(m1, m5) // true
   */
  ne (m1, m2) {
    return !this.eq(m1, m2)
  }

  /**
   * @desc
   * The LinearAlgebra#isZero method checks if `m` is zero.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {boolean}
   * `true` if `m` is zero and `false` otherwise.
   *
   * @throws {Error}
   * if `m` is LU-factorised.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * l.isZero(l.$(0, 0, 0, 0)) // true
   * l.isZero(l.$()) // true
   * l.isZero(l.$(1, 2)) // false
   *
   * let m = l.ilup(l.$(0, 0, 0, 0).setDim(2, 2))
   * l.isZero(m) // Error
   */
  isZero (m) {
    m = this.cast(m)
    if (m.isLUP()) {
      throw Error('LU-factorised matrices are not suppoted by isZero method.')
    }
    const salg = this.salg
    let flag = true
    for (let i = m.length - 1; flag && i >= 0; i -= 1) {
      flag = salg.isZero(m[i])
    }
    return flag
  }

  /**
   * @desc
   * The LinearAlgebra#isInteger method checks if all the elements of `m`
   * are integers.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {boolean}
   * `true` if all the elements of `m` are integers and `false` otherwise.
   *
   * @throws {Error}
   * if `m` is LU-factorised.
   *
   * @version 1.1.0
   * @since 1.1.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * l.isInteger([0, 0, 0, 0]) // true
   * l.isInteger([1, 3, -2, r.$(1, 2)]) // false
   *
   * let m = l.ilup(l.$(0, 0, 0, 0).setDim(2, 2))
   * l.isInteger(m) // Error
   */
  isInteger (m) {
    m = this.cast(m)
    if (m.isLUP()) {
      throw Error('LU-factorised matrices are not suppoted by isInteger ' +
          'method.')
    }
    const salg = this.salg
    let flag = true
    for (let i = m.length - 1; flag && i >= 0; i -= 1) {
      flag = salg.isInteger(m[i])
    }
    return flag
  }

  /**
   * @desc
   * The LinearAlgebra#isFinite method checks if `m` is finite.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {boolean}
   * `true` if `m` is finite and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import bigInt from 'big-integer'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let a = r.$(bigInt('1e999'))
   *
   * l.isFinite(l.$(0)) // true
   * l.isFinite(l.$(0, a, 1)) // true
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * import bigInt from 'big-integer'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let a = r.$(bigInt('1e999'))
   *
   * l.isFinite(l.$(0)) // true
   * l.isFinite(l.$(0, a, 1)) // false
   */
  isFinite (m) {
    m = this.cast(m)
    const salg = this.salg
    let flag = true
    for (let i = m.length - 1; flag && i >= 0; i -= 1) {
      flag = salg.isFinite(m[i])
    }
    return flag
  }

  /**
   * @desc
   * The LinearAlgebra#isExact method checks
   * if `this` is an implementation of exact algebra.
   *
   * @return {boolean}
   * `true` if `this` is an exact algebra and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * l.isExact() // true
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * l.isExact() // false
   */
  isExact () {
    return this.salg.isExact()
  }

  /**
   * @desc
   * The LinearAlgebra#isReal method checks
   * if `this` is an implementation of real algebra.
   *
   * @return {boolean}
   * `true` if `this.salg` is an implementation of real algebra
   * and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * l.isReal() // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * l.isReal() // false
   *
   * new LinearAlgebra(r).isReal() // true
   */
  isReal () {
    return this.salg.isReal()
  }

  /**
   * @desc
   * The LinearAlgebra#add method returns the result of
   * the element-wise addition `m1` plus `m2`.
   *
   * @param {Matrix} m1
   * a {@link Matrix}.
   *
   * @param {Matrix} m2
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * a new {@link Matrix}
   * representing the result of the element-wise addition `m1` plus `m2`.
   *
   * @throws {Error}
   * if the dimensions of `m1` and `m2` are not the same.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(1, 2)
   * let m2 = l.$(3, 4)
   * let m3 = l.add(m1, m2)
   *
   * m3 instanceof M // true
   * m1 !== m3 // true
   * l.eq(m1, l.$(1, 2)) // true
   * l.eq(m3, l.$(4, 6)) // true
   * l.add(m1, l.$(1, 2, 3)) // Error
   */
  add (m1, m2) {
    m1 = this.cast(m1)
    m2 = this.cast(m2)
    if (m1.hasSameDim(m2)) {
      return this.iadd(this.copy(m1), m2)
    } else {
      throw Error('the dimesions of `m1` and `m2` must be the same.')
    }
  }

  /**
   * @desc
   * The LinearAlgebra#iadd method adds `m2` to `m1` *in place* element-wise.
   *
   * @param {Matrix} m1
   * a {@link Matrix}.
   *
   * @param {Matrix} m2
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * `m1`, or a new instance of {@link Matrix} if `m1` is not an instance of
   * {@link Matrix}.
   *
   * @throws {Error}
   * if the dimensions of `m1` and `m2` are not the same.
   *
   * @throws {Error}
   * if `m1` or `m2` is LU-factorised.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(1, 2)
   * let m2 = l.$(3, 4)
   * let m3 = m1
   *
   * // GOOD-PRACTICE!
   * m1 = l.iadd(m1, m2)
   * m1 === m3 // true
   * m1 instanceof M // true
   * l.eq(m1, new M(4, 6)) // true
   *
   * m1 = new M(1, 2, 3, 4).setDim(2, 2)
   * m2 = new M(5, 7, 5, 3).setDim(2, 2)
   * l.iadd(l.lup(m1), m2) // Error
   * l.iadd(m1, l.lup(m2)) // Error
   * l.iadd(m1, l.$(1, 2, 3)) // Error
   */
  iadd (m1, m2) {
    m1 = this.cast(m1)
    m2 = this.cast(m2)
    if (m1.isLUP() || m2.isLUP()) {
      throw Error('LU-factorised matrices are not suppoted by isub method.')
    }
    if (m1.hasSameDim(m2)) {
      const salg = this.salg
      for (let i = m1.length - 1; i >= 0; i -= 1) {
        m1[i] = salg.iadd(m1[i], m2[i])
      }
      return m1
    } else {
      throw Error('the dimesions of `m1` and `m2` must be the same.')
    }
  }

  /**
   * @desc
   * The LinearAlgebra#sub method returns the result of
   * the element-wise subtraction `m1` minus `m2`.
   *
   * @param {Matrix} m1
   * a {@link Matrix}.
   *
   * @param {Matrix} m2
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * a new {@link Matrix}
   * representing the result of the element-wise subtraction `m1` minus `m2`.
   *
   * @throws {Error}
   * if the dimensions of `m1` and `m2` are not the same.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(1, 2)
   * let m2 = l.$(3, 4)
   * let m3 = l.sub(m1, m2)
   *
   * m3 instanceof M // true
   * m1 !== m3 // true
   * l.eq(m1, l.$(1, 2)) // true
   * l.eq(m3, l.$(-2, -2)) // true
   * l.sub(m1, l.$(1, 2, 3)) // Error
   */
  sub (m1, m2) {
    m1 = this.cast(m1)
    m2 = this.cast(m2)
    if (m1.hasSameDim(m2)) {
      return this.isub(this.copy(m1), m2)
    } else {
      throw Error('the dimesions of `m1` and `m2` must be the same.')
    }
  }

  /**
   * @desc
   * The LinearAlgebra#isub method subtracts `m2` from `m1` *in place*
   * element-wise.
   *
   * @param {Matrix} m1
   * a {@link Matrix}.
   *
   * @param {Matrix} m2
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * `m1`, or a new instance of {@link Matrix} if `m1` is not an instance of
   * {@link Matrix}.
   *
   * @throws {Error}
   * if the dimensions of `m1` and `m2` are not the same.
   *
   * @throws {Error}
   * if `m1` or `m2` is LU-factorised.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(1, 2)
   * let m2 = l.$(3, 4)
   * let m3 = m1
   *
   * // GOOD-PRACTICE!
   * m1 = l.isub(m1, m2)
   * m1 === m3 // true
   * m1 instanceof M // true
   * l.eq(m1, new M(-2, -2)) // true
   *
   * m1 = new M(1, 2, 3, 4).setDim(2, 2)
   * m2 = new M(5, 7, 5, 3).setDim(2, 2)
   * l.isub(l.lup(m1), m2) // Error
   * l.isub(m1, l.lup(m2)) // Error
   * l.isub(m1, l.$(1, 2, 3)) // Error
   */
  isub (m1, m2) {
    m1 = this.cast(m1)
    m2 = this.cast(m2)
    if (m1.isLUP() || m2.isLUP()) {
      throw Error('LU-factorised matrices are not suppoted by isub method.')
    }
    if (m1.hasSameDim(m2)) {
      const salg = this.salg
      for (let i = m1.length - 1; i >= 0; i -= 1) {
        m1[i] = salg.isub(m1[i], m2[i])
      }
      return m1
    } else {
      throw Error('the dimesions of `m1` and `m2` must be the same.')
    }
  }

  /**
   * @desc
   * The LinearAlgebra#mul method returns the result of
   * the element-wise multiplication `m1` times `m2`.
   *
   * @param {Matrix} m1
   * a {@link Matrix}.
   *
   * @param {Matrix} m2
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * a new {@link Matrix}
   * representing the result of the element-wise multiplication `m1` times `m2`.
   *
   * @throws {Error}
   * if the dimensions of `m1` and `m2` are not the same.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(1, 2)
   * let m2 = l.$(3, 4)
   * let m3 = l.mul(m1, m2)
   *
   * m3 instanceof M // true
   * m1 !== m3 // true
   * l.eq(m1, l.$(1, 2)) // true
   * l.eq(m3, l.$(3, 8)) // true
   * l.mul(m1, l.$(1, 2, 3)) // Error
   */
  mul (m1, m2) {
    m1 = this.cast(m1)
    m2 = this.cast(m2)
    if (m1.hasSameDim(m2)) {
      return this.imul(this.copy(m1), m2)
    } else {
      throw Error('the dimesions of `m1` and `m2` must be the same.')
    }
  }

  /**
   * @desc
   * The LinearAlgebra#imul method multiplies `m1` by `m2` *in place*
   * element-wise.
   *
   * @param {Matrix} m1
   * a {@link Matrix}.
   *
   * @param {Matrix} m2
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * `m1`, or a new instance of {@link Matrix} if `m1` is not an instance of
   * {@link Matrix}.
   *
   * @throws {Error}
   * if the dimensions of `m1` and `m2` are not the same.
   *
   * @throws {Error}
   * if `m1` or `m2` is LU-factorised.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(1, 2)
   * let m2 = l.$(3, 4)
   * let m3 = m1
   *
   * // GOOD-PRACTICE!
   * m1 = l.imul(m1, m2)
   * m1 === m3 // true
   * m1 instanceof M // true
   * l.eq(m1, l.$(3, 8)) // true
   *
   * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
   * m2 = l.$(5, 7, 5, 3).setDim(2, 2)
   * l.imul(l.lup(m1), m2) // Error
   * l.imul(m1, l.lup(m2)) // Error
   * l.imul(m1, l.$(1, 2, 3)) // Error
   */
  imul (m1, m2) {
    m1 = this.cast(m1)
    m2 = this.cast(m2)
    if (m1.isLUP() || m2.isLUP()) {
      throw Error('LU-factorised matrices are not suppoted by imul method.')
    }
    if (m1.hasSameDim(m2)) {
      const salg = this.salg
      for (let i = m1.length - 1; i >= 0; i -= 1) {
        m1[i] = salg.imul(m1[i], m2[i])
      }
      return m1
    } else {
      throw Error('the dimesions of `m1` and `m2` must be the same.')
    }
  }

  /**
   * @desc
   * The LinearAlgebra#div method returns the result of
   * the element-wise division `m1` over `m2`.
   *
   * @param {Matrix} m1
   * a {@link Matrix}.
   *
   * @param {Matrix} m2
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * a new {@link Matrix}
   * representing the result of the element-wise division `m1` times `m2`.
   *
   * @throws {Error}
   * if the dimensions of `m1` and `m2` are not the same.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(4, 6)
   * let m2 = l.$(2, 2)
   * let m3 = l.div(m1, m2)
   *
   * m3 instanceof M // true
   * m1 !== m3 // true
   * l.eq(m1, l.$(4, 6)) // true
   * l.eq(m3, l.$(2, 3)) // true
   * l.div(m1, l.$(1, 2, 3)) // Error
   */
  div (m1, m2) {
    m1 = this.cast(m1)
    m2 = this.cast(m2)
    if (m1.hasSameDim(m2)) {
      return this.idiv(this.copy(m1), m2)
    } else {
      throw Error('the dimesions of `m1` and `m2` must be the same.')
    }
  }

  /**
   * @desc
   * The LinearAlgebra#idiv method divides `m1` by `m2` *in place*
   * element-wise.
   *
   * @param {Matrix} m1
   * a {@link Matrix}.
   *
   * @param {Matrix} m2
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * `m1`, or a new instance of {@link Matrix} if `m1` is not an instance of
   * {@link Matrix}.
   *
   * @throws {Error}
   * if the dimensions of `m1` and `m2` are not the same.
   *
   * @throws {Error}
   * if `m1` or `m2` is LU-factorised.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(4, 6)
   * let m2 = l.$(2, 2)
   * let m3 = m1
   *
   * // GOOD-PRACTICE!
   * m1 = l.idiv(m1, m2)
   * m1 === m3 // true
   * m1 instanceof M // true
   * l.eq(m1, l.$(2, 3)) // true
   *
   * m1 = l.$(1, 2, 3, 4).setDim(2, 2)
   * m2 = l.$(5, 7, 5, 3).setDim(2, 2)
   * l.idiv(l.lup(m1), m2) // Error
   * l.idiv(m1, l.lup(m2)) // Error
   * l.idiv(m1, l.$(1, 2, 3)) // Error
   */
  idiv (m1, m2) {
    m1 = this.cast(m1)
    m2 = this.cast(m2)
    if (m1.isLUP() || m2.isLUP()) {
      throw Error('LU-factorised matrices are not suppoted by idiv method.')
    }
    if (m1.hasSameDim(m2)) {
      const salg = this.salg
      for (let i = m1.length - 1; i >= 0; i -= 1) {
        m1[i] = salg.idiv(m1[i], m2[i])
      }
      return m1
    } else {
      throw Error('the dimesions of `m1` and `m2` must be the same.')
    }
  }

  /**
   * @desc
   * The LinearAlgebra#neg method returns the result of
   * the element-wise multiplication `m` times `-1`.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * a new {@link Matrix}
   * representing the result of the element-wise multiplication `m` times `-1`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(4, 6)
   * let m2 = l.neg(m1)
   *
   * m2 instanceof M // true
   * m1 !== m2 // true
   * l.eq(m1, l.$(4, 6)) // true
   * l.eq(m2, l.$(-4, -6)) // true
   */
  neg (m) {
    return this.ineg(this.copy(m))
  }

  /**
   * @desc
   * The LinearAlgebra#ineg method multiplies `m` by `-1` *in place*
   * element-wise.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * `m`, or a new instance of {@link Matrix} if `m` is not an instance of
   * {@link Matrix}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(4, 6)
   * let m2 = m1
   *
   * // GOOD-PRACTICE!
   * m1 = l.ineg(m1)
   * m1 === m2 // true
   * m1 instanceof M // true
   * l.eq(m1, l.$(-4, -6)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let m2 = l.ilup(m1)
   * m2 = l.ineg(m2)
   *
   * m2 instanceof M // true
   * m2.isLUP() // true
   * r.eq(m2[0], -5) // true
   * r.eq(m2[1], r.$(4, 5)) // true
   * r.eq(m2[2], -5) // true
   * r.eq(m2[3], -3) // true
   */
  ineg (m) {
    m = this.cast(m)
    const salg = this.salg
    if (m.isLUP()) {
      const n = m.getDim()[0]
      for (let i = n - 1; i >= 0; i -= 1) {
        const ni = n * i
        for (let j = i; j >= 0; j -= 1) {
          const nij = ni + j
          m[nij] = salg.ineg(m[nij])
        }
      }
    } else {
      for (let i = m.length - 1; i >= 0; i -= 1) {
        m[i] = salg.ineg(m[i])
      }
    }
    return m
  }

  /**
   * @desc
   * The LinearAlgebra#cjg method returns the complex conjugate of `m`.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * a new {@link Matrix}
   * representing the complex conjugate of `m`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(4, c.$(0, 6), 2)
   * let m2 = l.cjg(m1)
   *
   * m2 instanceof M // true
   * m1 !== m2 // true
   * l.eq(m1, l.$(4, c.$(0, 6), 2)) // true
   * l.eq(m2, l.$(4, c.$(0, -6), 2)) // true
   */
  cjg (m) {
    return this.icjg(this.copy(m))
  }

  /**
   * @desc
   * The LinearAlgebra#icjg method
   * evaluates the complex conjugate of `m` and stores the result to `m`.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * `m`, or a new instance of {@link Matrix} if 'm' is not an instance of
   * {@link Matrix}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(4, c.$(0, 6), 2)
   * let m2 = m1
   *
   * // GOOD-PRACTICE!
   * m1 = l.icjg(m1)
   * m1 === m2 // true
   * m1 instanceof M // true
   * l.eq(m1, l.$(4, c.$(0, -6), 2)) // true
   */
  icjg (m) {
    m = this.cast(m)
    const salg = this.salg
    for (let i = m.length - 1; i >= 0; i -= 1) {
      m[i] = salg.icjg(m[i])
    }
    return m
  }

  /**
   * @desc
   * The LinearAlgebra#transpose method returns the transpose of `m`.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * a new {@link Matrix}
   * representing the transpose of `m`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(4, c.$(0, 6), 2, 3).setDim(2, 2)
   * let m2 = l.transpose(m1)
   *
   * m2 instanceof M // true
   * m1 !== m2 // true
   * l.eq(m1, l.$(4, c.$(0, 6), 2, 3).setDim(2, 2)) // true
   * l.eq(m2, l.$(4, 2, c.$(0, 6), 3).setDim(2, 2)) // true
   */
  transpose (m) {
    return this.itranspose(this.copy(m))
  }

  /**
   * @desc
   * The LinearAlgebra#itranspose method
   * calculates the transpose of `m` and stores the result to `m`.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * `m`, or a new instance of {@link Matrix} if `m` is not an instance of
   * {@link Matrix}.
   *
   * @throws {Error}
   * if `m` is LU-factorised.
   *
   * @throws {Error}
   * if the dimension of `m` is adaptive.
   *
   * @version 2.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(4, c.$(0, 6), 2, 3).setDim(2, 2)
   * let m2 = m1
   *
   * // GOOD-PRACTICE!
   * m1 = l.itranspose(m1)
   * m1 === m2 // true
   * m1 instanceof M // true
   * l.eq(m1, l.$(4, 2, c.$(0, 6), 3).setDim(2, 2)) // true
   *
   * m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * l.itranspose(l.lup(m1)) // Error
   * l.itranspose(l.$(5, 4, 5, 7)) // Error
   */
  itranspose (m) {
    m = this.cast(m)
    if (m.isLUP()) {
      throw Error('LU-factorised matrices are not suppoted by itranspose ' +
          'method.')
    }
    if (m.isAdaptive()) {
      throw Error('adaptive matrix cannot be transposed.')
    } else {
      const dim = m.getDim()
      const nRow = dim[0]
      const nCol = dim[1]
      const swap = m.slice()
      let k = m.length - 1
      for (let i = nRow - 1; i >= 0; i -= 1) {
        for (let j = nCol - 1; j >= 0; j -= 1, k -= 1) {
          const idx = i + j * nRow
          m[idx] = swap[k]
        }
      }
      m.setDim(m.nCol, m.nRow)
      return m
    }
  }

  /**
   * @desc
   * The LinearAlgebra#cjgTranspose method returns
   * the conjugate transpose of `m`.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * a new {@link Matrix}
   * representing the transpose of `m`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(4, c.$(0, 6), 2, 3).setDim(2, 2)
   * let m2 = l.cjgTranspose(m1)
   *
   * m2 instanceof M // true
   * m1 !== m2 // true
   * l.eq(m1, l.$(4, c.$(0, 6), 2, 3).setDim(2, 2)) // true
   * l.eq(m2, l.$(4, 2, c.$(0, -6), 3).setDim(2, 2)) // true
   */
  cjgTranspose (m) {
    return this.icjgTranspose(this.copy(m))
  }

  /**
   * @desc
   * The LinearAlgebra#icjgTranspose method
   * calculates the conjugate transpose of `m` and stores the result to `m`.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * `m`, or a new instance of {@link Matrix} if `m` is not an instance of
   * {@link Matrix}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(4, c.$(0, 6), 2, 3).setDim(2, 2)
   * let m2 = m1
   *
   * // GOOD-PRACTICE!
   * m1 = l.icjgTranspose(m1)
   * m1 === m2 // true
   * m1 instanceof M // true
   * l.eq(m1, l.$(4, 2, c.$(0, -6), 3).setDim(2, 2)) // true
   */
  icjgTranspose (m) {
    return this.icjg(this.itranspose(m))
  }

  /**
   * @desc
   * The LinearAlgebra#abs method returns the absolute value
   * (Frobenius norm) of `m`.
   *
   * CAUTION: this method is not implemented for exact algebra.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {RealAlgebraicElement}
   * a {@link RealAlgebraicElement}
   * representing the absolute value (Frobenius norm) of `m`.
   *
   * @throws {Error}
   * if `this` is an implementation of exact algebra.
   *
   * @throws {Error}
   * if `m` is LU-factorised.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m = l.$(1, -1)
   *
   * l.abs(m) // Error
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m = new M(1, -1)
   *
   * let a = l.abs(m)
   * a instanceof M // false
   * typeof a === 'number' // true
   * l.eq(m, new M(1, -1)) // true
   * r.eq(a, Math.sqrt(2)) // true
   *
   * m = new M(5, 4, 5, 7).setDim(2, 2)
   * l.abs(l.lup(m)) // Error
   */
  abs (m) {
    m = this.cast(m)
    if (m.isLUP()) {
      throw Error('LU-factorised matrices are not suppoted by abs method.')
    }
    if (this.isExact()) {
      throw Error('`abs` is not implemented for exact algebra.')
    } else {
      return Math.sqrt(this.abs2(m))
    }
  }

  /**
   * @desc
   * The LinearAlgebra#abs2 method returns
   * the square of the absolute value (Frobenius norm) of `m`.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {RealAlgebraicElement}
   * a {@link RealAlgebraicElement}
   * representing the square of the absolute value (Frobenius nomr) of `m`.
   *
   * @throws {Error}
   * if `m` is LU-factorised.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m = l.$(1, -1)
   *
   * let a = l.abs2(m)
   * a instanceof M // false
   * a instanceof P // true
   * l.eq(m, l.$(1, -1)) // true
   * r.eq(a, 2) // true
   *
   * m = l.$(5, 4, 5, 7).setDim(2, 2)
   * l.abs2(l.lup(m)) // Error
   */
  abs2 (m) {
    m = this.cast(m)
    if (m.isLUP()) {
      throw Error('LU-factorised matrices are not suppoted by abs2 method.')
    }
    const salg = this.salg
    const ralg = salg.ralg
    let sum = ralg.$(0)
    for (let i = m.length - 1; i >= 0; i -= 1) {
      sum = ralg.iadd(sum, salg.abs2(m[i]))
    }
    return sum
  }

  /**
   * @desc
   * The LinearAlgebra#dot method returns
   * the dot product of `m1` and `m2` (as vectors).
   *
   * @param {Matrix} m1
   * a {@link Matrix}.
   *
   * @param {Matrix} m2
   * a {@link Matrix}.
   *
   * @return {ScalarAlgebraicElement}
   * a {@link ScalarAlgebraicElement}
   * representing the dot product of `m1` and `m2` (as vectors).
   *
   * @throws {Error}
   * if `m1` and `m2` are not the same dimension.
   *
   * @throws {Error}
   * if `m1` or `m2` is LU-factorised.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra, ComplexAlgebraicElement as C }
   *   from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(1, c.$(0, 1))
   * let m2 = l.$(1, c.$(0, 1))
   * let m3 = l.$(1, c.$(0, -1))
   *
   * let z = l.dot(m1, m2)
   * z instanceof M // false
   * z instanceof C // true
   * l.eq(m1, l.$(1, c.$(0, 1))) // true
   * c.eq(z, 2) // true
   *
   * let w = l.dot(m1, m3)
   * w instanceof M // false
   * w instanceof C // true
   * c.eq(w, 0) // true
   *
   * m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * m2 = l.$(1, 2, 3, 4).setDim(2, 2)
   * l.dot(m1, l.lup(m2)) // Error
   * l.dot(l.lup(m1), m2) // Error
   * l.dot(l.$(1, 2), l.$(1)) // Error
   */
  dot (m1, m2) {
    m1 = this.cast(m1)
    m2 = this.cast(m2)
    if (m1.isLUP() || m2.isLUP()) {
      throw Error('LU-factorised matrices are not suppoted by dot method.')
    }
    if (m1.hasSameDim(m2)) {
      const salg = this.salg
      let sum = salg.$(0)
      for (let i = m1.length - 1; i >= 0; i -= 1) {
        sum = salg.iadd(sum, salg.imul(salg.cjg(m1[i]), m2[i]))
      }
      return sum
    } else {
      throw Error('the dimesions of `m1` and `m2` must be the same.')
    }
  }

  /**
   * @desc
   * The LinearAlgebra#smul method returns
   * the product of `m` and a scalar `s`.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @param {ScalarAlgebraicElement} s
   * a {@link ScalarAlgebraicElement}.
   *
   * @return {Matrix}
   * a new {@link Matrix}
   * representing the product of `m` and a scalar `s`.
   *
   * @version 1.0.1
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(1, c.$(0, 1))
   * let m2 = l.smul(m1, c.$(0, 2))
   *
   * m2 instanceof M // true
   * m2 !== m1 // true
   * l.eq(m1, l.$(1, c.$(0, 1))) // true
   * l.eq(m2, l.$(c.$(0, 2), -2)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let m2 = l.ilup(m1)
   * let m3 = l.smul(m2, 2)
   *
   * m3 instanceof M // true
   * m3.isLUP() // true
   * r.eq(m3[0], 10) // true
   * r.eq(m3[1], r.$(4, 5)) // true
   * r.eq(m3[2], 10) // true
   * r.eq(m3[3], 6) // true
   */
  smul (m, s) {
    return this.ismul(this.copy(m), s)
  }

  /**
   * @desc
   * The LinearAlgebra#ismul method multiplies `m` by
   * a scalar `s` *in place*.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @param {ScalarAlgebraicElement} s
   * a {@link ScalarAlgebraicElement}.
   *
   * @return {Matrix}
   * `m`, or a new instance of {@link Matrix} if `m` is not an instance of
   * {@link Matrix}.
   *
   * @version 1.0.1
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(1, c.$(0, 1))
   * let m2 = m1
   * m1 = l.ismul(m1, c.$(0, 2))
   *
   * m1 instanceof M // true
   * m1 === m2 // true
   * l.eq(m1, l.$(c.$(0, 2), -2)) // true
   * l.eq(m2, l.$(c.$(0, 2), -2)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let m2 = l.ilup(m1)
   * m2 = l.ismul(m2, 2)
   *
   * m2 instanceof M // true
   * m2.isLUP() // true
   * r.eq(m2[0], 10) // true
   * r.eq(m2[1], r.$(4, 5)) // true
   * r.eq(m2[2], 10) // true
   * r.eq(m2[3], 6) // true
   */
  ismul (m, s) {
    m = this.cast(m)
    const salg = this.salg
    s = salg.cast(s)
    if (m.isLUP()) {
      const n = m.getDim()[0]
      for (let i = n - 1; i >= 0; i -= 1) {
        const ni = n * i
        for (let j = i; j >= 0; j -= 1) {
          const nij = ni + j
          m[nij] = salg.imul(m[nij], s)
        }
      }
    } else {
      for (let i = m.length - 1; i >= 0; i -= 1) {
        m[i] = salg.imul(m[i], s)
      }
    }
    return m
  }

  /**
   * @desc
   * The LinearAlgebra#sdiv method returns the result of the division `m` over
   * `s`, where `s` is a scalar.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @param {ScalarAlgebraicElement} s
   * a {@link ScalarAlgebraicElement}.
   *
   * @return {Matrix}
   * a new {@link Matrix}
   * representing the division `m` over `s`.
   *
   * @version 2.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(1, c.$(0, 1))
   * let m2 = l.sdiv(m1, c.$(0, 2))
   *
   * m2 instanceof M // true
   * m2 !== m1 // true
   * l.eq(m1, l.$(1, c.$(0, 1))) // true
   * l.eq(m2, l.$(c.$(0, r.$(-1, 2)), r.$(1, 2))) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let m2 = l.ilup(m1)
   * let m3 = l.sdiv(m2, r.$(1, 2))
   *
   * m3 instanceof M // true
   * m3.isLUP() // true
   * r.eq(m3[0], 10) // true
   * r.eq(m3[1], r.$(4, 5)) // true
   * r.eq(m3[2], 10) // true
   * r.eq(m3[3], 6) // true
   */
  sdiv (m, s) {
    return this.isdiv(this.copy(m), s)
  }

  /**
   * @desc
   * The LinearAlgebra#isdiv method divides `m` by a scalar `s` *in place*.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @param {ScalarAlgebraicElement} s
   * a {@link ScalarAlgebraicElement}.
   *
   * @return {Matrix}
   * `m`, or a new instance of {@link Matrix} if `m` is not an instance of
   * {@link Matrix}.
   *
   * @version 2.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(1, c.$(0, 1))
   * let m2 = m1
   * m1 = l.isdiv(m1, c.$(0, 2))
   *
   * m1 instanceof M // true
   * m1 === m2 // true
   * l.eq(m1, l.$(c.$(0, r.$(-1, 2)), r.$(1, 2))) // true
   * l.eq(m2, l.$(c.$(0, r.$(-1, 2)), r.$(1, 2))) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let m2 = l.ilup(m1)
   * m2 = l.isdiv(m2, r.$(1, 2))
   *
   * m2 instanceof M // true
   * m2.isLUP() // true
   * r.eq(m2[0], 10) // true
   * r.eq(m2[1], r.$(4, 5)) // true
   * r.eq(m2[2], 10) // true
   */
  isdiv (m, s) {
    m = this.cast(m)
    const salg = this.salg
    s = salg.cast(s)
    if (m.isLUP()) {
      const n = m.getDim()[0]
      for (let i = n - 1; i >= 0; i -= 1) {
        const ni = n * i
        for (let j = i; j >= 0; j -= 1) {
          const nij = ni + j
          m[nij] = salg.idiv(m[nij], s)
        }
      }
    } else {
      for (let i = m.length - 1; i >= 0; i -= 1) {
        m[i] = salg.idiv(m[i], s)
      }
    }
    return m
  }

  /**
   * @desc
   * The LinearAlgebra#mmul method returns the matrix product of `m1` and `m2`.
   *
   * If `m1` is adaptive, `m1.nCol` is assumed to be equal to `m2.nRow`.
   *
   * If `m2` is adaptive, `m2.nRow` is assumed to be equal to `m1.nCol`.
   *
   * If `m1` or `m2` is adaptive, the product is also adaptive,
   * otherwise `nRow` and `nCol` of the product are set at `m1.nRow` and
   * `m2.nCol`, respectively.
   *
   * @param {Matrix} m1
   * a {@link Matrix}.
   *
   * @param {Matrix} m2
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * a new {@link Matrix} representing the matrix product of `m1` and `m2`.
   *
   * @throws {Error}
   * if `m1` or `m2` is LU-factorised.
   *
   * @throws {Error}
   * if the dimensions of both `m1` and `m2` are adaptive.
   *
   * @throws {Error}
   * if `nCol` of `m1` and `nRow` of `m2` are not the same.
   *
   * @version 2.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(1, c.$(0, 1), 3, 4).setDim(2, 0)
   * let m2 = l.$(1, 2, 3, 4).setDim(2, 0)
   * let m3 = l.mmul(m1, m2)
   * let m4 = l.$(c.$(1, 3), c.$(2, 4), 15, 22)
   * m4.setDim(2, 0)
   *
   * m3 instanceof M // true
   * l.eq(m1, l.$(1, c.$(0, 1), 3, 4).setDim(0, 2)) // true
   * l.eq(m2, l.$(1, 2, 3, 4).setDim(2, 0)) // true
   * l.eq(m3, m4) // true
   * m3.nRow // 2
   * m3.nCol // 0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(1, c.$(0, 1), 3, 4).setDim(0, 2)
   * let m2 = l.$(1, 2, 3, 4).setDim(2, 0)
   * let m3 = l.mmul(m1, m2)
   * let m4 = l.$(c.$(1, 3), c.$(2, 4), 15, 22)
   *
   * m3 instanceof M // true
   * l.eq(m1, l.$(1, c.$(0, 1), 3, 4).setDim(0, 2)) // true
   * l.eq(m2, l.$(1, 2, 3, 4).setDim(2, 0)) // true
   * l.eq(m3, m4) // true
   * m3.nRow // 0
   * m3.nCol // 0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(1, c.$(0, 1))
   * let m2 = l.$(1, 2, 3, 4).setDim(2, 0)
   * let m3 = l.mmul(m1, m2)
   * let m4 = l.mmul(m2, m1)
   *
   * l.eq(m1, l.$(1, c.$(0, 1))) // true
   * l.eq(m2, l.$(1, 2, 3, 4).setDim(2, 0)) // true
   * m3 instanceof M // true
   * m4 instanceof M // true
   * m3.isAdaptive() // true
   * m4.isAdaptive() // true
   * l.eq(m3, l.$(c.$(1, 3), c.$(2, 4))) // true
   * l.eq(m4, l.$(c.$(1, 2), c.$(3, 4))) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * l.mmul(l.$(), l.$()).toString() // '()'
   * let m1 = l.$(1, 5, 3, 4).setDim(2, 0)
   * let m2 = l.$(1, 2, 3, 4).setDim(2, 0)
   * l.mmul(l.lup(m1), m2) // Error
   * l.mmul(m1, l.lup(m2)) // Error
   * l.mmul(l.$(1, 5).setDim(1, 2), l.$(1, 5).setDim(1, 2)) // Error
   * l.mmul(l.$(1, 5), l.$(1, 5)) // Error
   */
  mmul (m1, m2) {
    m1 = this.cast(m1)
    m2 = this.cast(m2)
    if (m1.isLUP() || m2.isLUP()) {
      throw Error('LU-factorised matrices are not suppoted by mmul method.')
    }
    let dim1
    let dim2
    let adaptive
    if (m1.isAdaptive()) {
      if (m2.isAdaptive()) {
        if (m1.length === 0 && m2.length === 0) {
          return new Matrix()
        } else {
          throw Error('dimensions of `m1` and `m2` cannot be determined.')
        }
      } else {
        dim2 = m2.getDim()
        dim1 = m1.getDim(0, dim2[0])
        adaptive = true
      }
    } else if (m2.isAdaptive()) {
      dim1 = m1.getDim()
      dim2 = m2.getDim(dim1[1], 0)
      adaptive = true
    } else {
      dim1 = m1.getDim()
      dim2 = m2.getDim()
      adaptive = false
    }
    const nCol1 = dim1[1]
    const nRow2 = dim2[0]
    if (nCol1 === nRow2) {
      const salg = this.salg
      const nRow1 = dim1[0]
      const nCol2 = dim2[1]
      const m3 = new Matrix()
      if (!adaptive) {
        m3.setDim(m1.nRow, m2.nCol)
      }
      for (let i = 0; i < nRow1; i += 1) {
        const ii = i * nCol1
        for (let j = 0; j < nCol2; j += 1) {
          let sum = salg.$(0)
          for (let k = nCol1 - 1; k >= 0; k -= 1) {
            sum = salg.iadd(sum, salg.mul(m1[ii + k], m2[k * nCol2 + j]))
          }
          m3.push(sum)
        }
      }
      return m3
    } else {
      throw Error('`nCol` of `m1` and `nRow` of `m2` must be the same.')
    }
  }

  /**
   * @desc
   * The LinearAlgebra#lup method calculates
   * LU-factorisation of `m` of the form *Pm* = *LU*.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * a new {@link Matrix} representing the LU-factorisation of `m`.
   *
   * @throws {Error}
   * if `m` cannot be interpreted as a square matrix.
   *
   * @version 2.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let m2 = l.lup(m1)
   *
   * m1 !== m2 // true
   * m2 instanceof M // true
   * m1.permutation === null // true
   * m2.permutation !== null // true
   * m2.permutation.length === 0 // true
   * r.eq(m2[0], 5) // true
   * r.eq(m2[1], r.$(4, 5)) // true
   * r.eq(m2[2], 5) // true
   * r.eq(m2[3], 3) // true
   *
   * l.lup(l.$(5, 4, 5, 7)).getDim()[0] // 2
   * l.lup(l.$(5, 4, 5, 7).setDim(1, 4)) // Error
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(0, 3, 5, 4).setDim(2, 2)
   * let m2 = l.lup(m1)
   *
   * m1 !== m2 // true
   * m2 instanceof M // true
   * m1.permutation === null // true
   * m2.permutation !== null // true
   * m2.permutation.length === 2 // true
   * m2.permutation[0] === 0 // true
   * m2.permutation[1] === 1 // true
   * r.eq(m2[0], 5) // true
   * r.eq(m2[1], r.$(4, 5)) // true
   * r.eq(m2[2], 0) // true
   * r.eq(m2[3], 3) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(1, 2, 1, 2).setDim(2, 2)
   * let m2 = l.lup(m1)
   *
   * m1 !== m2 // true
   * m2 instanceof M // true
   * m2.permutation !== null // true
   * r.eq(m2[0], 0) // true
   * r.eq(m2[3], 0) // true
   *
   * let m3 = l.lup(m2)
   * m1 !== m3 // true
   * m2 !== m3 // true
   * m3 instanceof M // true
   * m3.permutation !== null // true
   * r.eq(m3[0], 0) // true
   * r.eq(m3[3], 0) // true
   *
   * l.lup(l.$(1, 2, 3)) // Error
   */
  lup (m) {
    m = this.cast(m)
    if (m.isLUP()) {
      return this.copy(m)
    } else if (m.isAdaptive()) {
      const dim = Math.round(Math.sqrt(m.length))
      if (dim * dim === m.length) {
        const m2 = this.copy(m).setDim(dim)
        return this.ilup(m2)
      } else {
        throw Error('m cannot be interpreted as a square matrix.')
      }
    } else if (m.isSquare()) {
      return this.ilup(this.copy(m))
    } else {
      throw Error('m must be a square matrix.')
    }
  }

  /**
   * @desc
   * The LinearAlgebra#ilup method calculates
   * LU-factorisation of `m` of the form *Pm* = *LU* *in-place*.
   * If `m` is a singular matrix,
   * `m[0]` and `m[n * n - 1]` is set at zero,
   * i.e. this method does not return a correct LU-factorised matrix,
   * but the determinant (as calculated by det method) is correct (= 0).
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * `m`, or a new instance of {@link Matrix} if `m` is not an instance of
   * {@link Matrix}.
   *
   * @throws {Error}
   * if `m` cannot be interpreted as a square matrix.
   *
   * @version 2.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let m2 = l.ilup(m1)
   *
   * m1 === m2 // true
   * m2 instanceof M // true
   * m2.permutation !== null // true
   * m2.permutation.length === 0 // true
   * r.eq(m2[0], 5) // true
   * r.eq(m2[1], r.$(4, 5)) // true
   * r.eq(m2[2], 5) // true
   * r.eq(m2[3], 3) // true
   *
   * l.ilup(l.$(5, 4, 5, 7)).getDim()[0] // 2
   * l.ilup(l.$(5, 4, 5, 7).setDim(1, 4)) // Error
   * l.ilup(l.$(5, 4, 5)) // Error
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(0, 3, 5, 4).setDim(2, 2)
   * let m2 = l.ilup(m1)
   *
   * m1 === m2 // true
   * m2 instanceof M // true
   * m2.permutation !== null // true
   * m2.permutation.length === 2 // true
   * m2.permutation[0] === 0 // true
   * m2.permutation[1] === 1 // true
   * r.eq(m2[0], 5) // true
   * r.eq(m2[1], r.$(4, 5)) // true
   * r.eq(m2[2], 0) // true
   * r.eq(m2[3], 3) // true
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra(1e-5)
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(0, 3, 5, 4).setDim(2, 2)
   * let m2 = l.ilup(m1)
   * let m3 = l.ilup(m2)
   *
   * m2 === m3 // true
   *
   * m1 === m2 // true
   * m2 instanceof M // true
   * m2.permutation !== null // true
   * m2.permutation.length === 2 // true
   * m2.permutation[0] === 0 // true
   * m2.permutation[1] === 1 // true
   * r.eq(m2[0], 5) // true
   * r.eq(m2[1], r.$(4, 5)) // true
   * r.eq(m2[2], 0) // true
   * r.eq(m2[3], 3) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(0, 2, 3, 0, 2, 3, 0, 2, 3).setDim(3, 3)
   * let m2 = l.ilup(m1)
   *
   * m1 === m2 // true
   * m2 instanceof M // true
   * m2.permutation !== null // true
   * r.eq(m2[0], 0) // true
   * r.eq(m2[8], 0) // true
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra(1e-5)
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(1, 2, 1, 2).setDim(2, 2)
   * let m2 = l.ilup(m1)
   *
   * m1 === m2 // true
   * m2 instanceof M // true
   * m2.permutation !== null // true
   * r.eq(m2[0], 0) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(1, 0, 0, 0, 1, 0, 0, 0, 1).setDim(3, 3)
   * let m2 = l.ilup(m1)
   *
   * m1 === m2 // true
   * m2 instanceof M // true
   * m2.permutation !== null // true
   * m2.permutation.length === 0 // true
   * r.eq(m2[0], 1) // true
   * r.eq(m2[1], 0) // true
   * r.eq(m2[2], 0) // true
   * r.eq(m2[3], 0) // true
   * r.eq(m2[4], 1) // true
   * r.eq(m2[5], 0) // true
   * r.eq(m2[6], 0) // true
   * r.eq(m2[7], 0) // true
   * r.eq(m2[8], 1) // true
   */
  ilup (m) {
    m = this.cast(m)
    if (m.isLUP()) {
      return m
    } else if (m.isAdaptive()) {
      const dim = Math.round(Math.sqrt(m.length))
      if (dim * dim === m.length) {
        m.setDim(dim)
      } else {
        throw Error('m cannot be interpreted as a square matrix.')
      }
    }
    if (m.isSquare()) {
      const salg = this.salg
      const ralg = salg.ralg
      const p = m.permutation = []
      const n = m.getDim()[0]
      for (let i = 0; i < n; i += 1) {
        const ni = n * i
        if (this.isExact()) {
          if (salg.isZero(m[ni + i])) {
            for (let k = i + 1; k < n; k += 1) {
              if (!salg.isZero(m[n * k + i])) {
                m.swapRow(i, k)
                p.push(i, k)
                break
              }
              if (k === n - 1) {
                // singular, returns a zero determinant matrix
                m[0] = salg.num(0)
                m[n * n - 1] = salg.num(0)
                return m
              }
            }
          }
        } else {
          let maxabs = salg.abs(m[ni + i])
          let maxk = i
          for (let k = i + 1; k < n; k += 1) {
            const abs = salg.abs(m[n * k + i])
            if (ralg.isPositive(ralg.sub(abs, maxabs))) {
              maxabs = abs
              maxk = k
            }
          }
          if (salg.isZero(maxabs)) {
            // singular, returns a zero determinant matrix
            m[0] = salg.num(0)
            m[n * n - 1] = salg.num(0)
            return m
          }
          if (i !== maxk) {
            m.swapRow(i, maxk)
            p.push(i, maxk)
          }
        }
        for (let j = i + 1; j < n; j += 1) {
          const idx = ni + j
          for (let k = i - 1; k >= 0; k -= 1) {
            m[idx] = salg.isub(m[idx], salg.mul(m[ni + k], m[n * k + j]))
          }
          m[idx] = salg.idiv(m[idx], m[ni + i])
        }
        {
          const j = i + 1
          for (let k = j; k < n; k += 1) {
            const nk = n * k
            const idx = nk + j
            for (let l = j - 1; l >= 0; l -= 1) {
              m[idx] = salg.isub(m[idx], salg.mul(m[nk + l], m[n * l + j]))
            }
          }
        }
      }
      if (salg.isZero(m[n * n - 1])) {
        // singular, returns a zero determinant matrix
        m[0] = salg.num(0)
      }
      return m
    } else {
      throw Error('m must be a square matrix.')
    }
  }

  /**
   * @desc
   * The LinearAlgebra#solve method calculates
   * either `m1^-1 m2` (if `m1` is LU-factorised)
   * or `m1 m2^-1` (if `m2` is LU-factorised).
   *
   * @param {Matrix} m1
   * a {@link Matrix}.
   *
   * @param {Matrix} m2
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * a new {@link Matrix} representing the result.
   *
   * @throws {Error}
   * if both `m1` and `m2` are LU-factorised.
   *
   * @throws {Error}
   * if neither `m1` nor `m2` is LU-factorised.
   *
   * @throws {Error}
   * if `nCol` of `m1` and `nRow` of `m2` is not the same.
   *
   * @version 2.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let m2 = l.lup(m1)
   * let m3 = l.solve(m1, m2)
   *
   * m3 instanceof M // true
   * m3 !== m1 // true
   * m3 !== m2 // true
   * m3.permutation === null // true
   * r.eq(m3[0], 1) // true
   * r.eq(m3[1], 0) // true
   * r.eq(m3[2], 0) // true
   * r.eq(m3[3], 1) // true
   *
   * let m4 = l.$(5, 4, 5, 7)
   * let m5 = l.solve(m4, m2)
   * m5 instanceof M // true
   * m5 !== m4 // true
   * m5 !== m2 // true
   * m5.permutation === null // true
   * r.eq(m5[0], 1) // true
   * r.eq(m5[1], 0) // true
   * r.eq(m5[2], 0) // true
   * r.eq(m5[3], 1) // true
   *
   * let m6 = l.$(5, 4, 5, 7)
   * let m7 = l.solve(m2, m6)
   * m7 instanceof M // true
   * m7 !== m6 // true
   * m7 !== m2 // true
   * m7.permutation === null // true
   * r.eq(m7[0], 1) // true
   * r.eq(m7[1], 0) // true
   * r.eq(m7[2], 0) // true
   * r.eq(m7[3], 1) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let m2 = l.lup(m1)
   * let m3 = l.solve(m2, m1)
   *
   * m3 instanceof M // true
   * m3 !== m1 // true
   * m3 !== m2 // true
   * m3.permutation === null // true
   * r.eq(m3[0], 1) // true
   * r.eq(m3[1], 0) // true
   * r.eq(m3[2], 0) // true
   * r.eq(m3[3], 1) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let m2 = l.copy(m1)
   * l.solve(m1, m2) // Error
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let m2 = l.lup(m1)
   * m1 = l.ilup(m1)
   * l.solve(m1, m2) // Error
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(1, 2, 1, 2).setDim(2, 2)
   * let m2 = l.lup(m1)
   * l.solve(m2, m1) // Error
   * l.solve(m1, m2) // Error
   * l.solve(m2, l.$(1, 2, 1).setDim(1, 3)) // Error
   * l.solve(l.$(1, 2, 1).setDim(1, 3), m2) // Error
   */
  solve (m1, m2) {
    m1 = this.cast(m1)
    m2 = this.cast(m2)
    if (m1.isLUP()) {
      if (m2.isLUP()) {
        throw Error('both `m1` and `m1` are LU-factorised.')
      } else {
        const dim1 = m1.getDim()
        const dim2 = m2.isAdaptive() ? m2.getDim(dim1[1], 0) : m2.getDim()
        if (dim1[1] !== dim2[0]) {
          throw Error('`nCol` of `m1` and `nRow` of `m2` cannot be ' +
            'interpreted as the same.')
        } else {
          return this.isolve(m1, this.copy(m2))
        }
      }
    } else if (m2.isLUP()) {
      const dim2 = m2.getDim()
      const dim1 = m1.isAdaptive() ? m1.getDim(0, dim2[0]) : m1.getDim()
      if (dim2[0] !== dim1[1]) {
        throw Error('`nCol` of `m1` and `nRow` of `m2` cannot be ' +
          'interpreted as the same.')
      } else {
        return this.isolve(this.copy(m1), m2)
      }
    } else {
      throw Error('neither `m1` nor `m2` is LU-factorised.')
    }
  }

  /**
   * @desc
   * The LinearAlgebra#isolve method calculates
   * either `m1^-1 m2` (if `m1` is LU-factorised)
   * or `m1 m2^-1` (if `m2` is LU-factorised) *in place*.
   * The results are stored to `m1` if `m2` is LU-factorised
   * or otherwise to `m2`.
   *
   * @param {Matrix} m1
   * a {@link Matrix}.
   *
   * @param {Matrix} m2
   * a {@link Matrix}.
   *
   * @return {Matrix}
   * `m1` if `m2` is LU-factorised or otherwise 'm2'.
   * If `m1` or `m2` that is to be returned is not an instance of
   * {@link Matrix}, a new instance of {@link Matrix} is returned.
   *
   * @throws {Error}
   * if both `m1` and `m2` are LU-factorised.
   *
   * @throws {Error}
   * if neither `m1` nor `m2` is LU-factorised.
   *
   * @throws {Error}
   * if `nCol` of `m1` and `nRow` of `m2` is not the same.
   *
   * @throws {Error}
   * if the LU-factorised matrix is singular.
   *
   * @version 2.1.1
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let m2 = l.lup(m1)
   * let m3 = l.isolve(m1, m2)
   *
   * m3 instanceof M // true
   * m3 === m1 // true
   * m3 !== m2 // true
   * m3.permutation === null // true
   * r.eq(m3[0], 1) // true
   * r.eq(m3[1], 0) // true
   * r.eq(m3[2], 0) // true
   * r.eq(m3[3], 1) // true
   *
   * let m4 = l.$(5, 4, 5, 7)
   * let m5 = l.isolve(m4, m2)
   * m5 instanceof M // true
   * m5 === m4 // true
   * m5 !== m2 // true
   * m5.permutation === null // true
   * r.eq(m5[0], 1) // true
   * r.eq(m5[1], 0) // true
   * r.eq(m5[2], 0) // true
   * r.eq(m5[3], 1) // true
   *
   * let m6 = l.$(5, 4, 5, 7)
   * let m7 = l.isolve(m2, m6)
   * m7 instanceof M // true
   * m7 === m6 // true
   * m7 !== m2 // true
   * m7.permutation === null // true
   * r.eq(m7[0], 1) // true
   * r.eq(m7[1], 0) // true
   * r.eq(m7[2], 0) // true
   * r.eq(m7[3], 1) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra, Matrix as M } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(0, 4, 5, 7).setDim(2, 2)
   * let m2 = l.lup(m1)
   * let m3 = l.isolve(m1, m2)
   *
   * m3 instanceof M // true
   * m3 === m1 // true
   * m3 !== m2 // true
   * m3.permutation === null // true
   * r.eq(m3[0], 1) // true
   * r.eq(m3[1], 0) // true
   * r.eq(m3[2], 0) // true
   * r.eq(m3[3], 1) // true
   *
   * m1 = l.$(0, 4, 5, 7).setDim(2, 2)
   * m3 = l.isolve(m2, m1)
   *
   * m3 instanceof M // true
   * m3 === m1 // true
   * m3 !== m2 // true
   * m3.permutation === null // true
   * r.eq(m3[0], 1) // true
   * r.eq(m3[1], 0) // true
   * r.eq(m3[2], 0) // true
   * r.eq(m3[3], 1) // true
   *
   * l.isolve(l.$(0, 4, 5).setDim(1, 3), m2) // Error
   * l.isolve(m2, l.$(0, 4, 5).setDim(1, 3)) // Error
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let m2 = l.copy(m1)
   * l.isolve(m1, m2) // Error
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let m2 = l.lup(m1)
   * m1 = l.ilup(m1)
   * l.isolve(m1, m2) // Error
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m1 = l.$(1, 2, 1, 2).setDim(2, 2)
   * let m2 = l.lup(m1)
   * l.isolve(m2, m1) // Error
   * l.isolve(m1, m2) // Error
   */
  isolve (m1, m2) {
    m1 = this.cast(m1)
    m2 = this.cast(m2)
    if (m1.isLUP()) {
      if (m2.isLUP()) {
        throw Error('both `m1` and `m1` are LU-factorised.')
      }
      const dim1 = m1.getDim()
      const dim = m2.isAdaptive() ? m2.getDim(dim1[1], 0) : m2.getDim()
      const n = dim[0]
      if (dim1[1] !== n) {
        throw Error('`nCol` of `m1` and `nRow` of `m2` cannot be interpreted ' +
          'as the same.')
      } else {
        const p = m1.permutation
        // ad hoc fix ...
        const flag = m2.isAdaptive()
        if (flag) {
          m2.setDim(dim1[1])
        }
        for (let i = 0, np = p.length; i < np; i += 2) {
          m2.swapRow(p[i], p[i + 1])
        }
        if (flag) {
          m2.setDim(0)
        }
        const salg = this.salg
        const nCol = dim[1]
        for (let i = 0; i < n; i += 1) {
          const ni1 = n * i
          const m1ii = m1[ni1 + i]
          if (salg.isZero(m1ii)) {
            throw Error('`m1` is singular.')
          }
          const ni2 = nCol * i
          for (let j = 0; j < nCol; j += 1) {
            const nij = ni2 + j
            for (let k = i - 1; k >= 0; k -= 1) {
              m2[nij] = salg.isub(m2[nij],
                salg.mul(m1[ni1 + k], m2[nCol * k + j]))
            }
            m2[nij] = salg.idiv(m2[nij], m1ii)
          }
        }
        for (let i = n - 1; i >= 0; i -= 1) {
          const ni1 = n * i
          const ni2 = nCol * i
          for (let j = 0; j < nCol; j += 1) {
            const nij = ni2 + j
            for (let k = i + 1; k < n; k += 1) {
              m2[nij] = salg.isub(m2[nij],
                salg.mul(m1[ni1 + k], m2[nCol * k + j]))
            }
          }
        }
        return m2
      }
    } else if (m2.isLUP()) {
      const dim2 = m2.getDim()
      const dim = m1.isAdaptive() ? m1.getDim(0, dim2[0]) : m1.getDim()
      const n = dim[1]
      if (n !== dim2[0]) {
        throw Error('`nCol` of `m1` and `nRow` of `m2` cannot be interpreted ' +
          'as the same.')
      } else {
        const salg = this.salg
        const nRow = dim[0]
        for (let i = 0; i < nRow; i += 1) {
          const ni = n * i
          for (let j = 0; j < n; j += 1) {
            const nij = ni + j
            for (let k = j - 1; k >= 0; k -= 1) {
              m1[nij] = salg.isub(m1[nij], salg.mul(m1[ni + k], m2[n * k + j]))
            }
          }
        }
        for (let i = 0; i < nRow; i += 1) {
          const ni = n * i
          for (let j = n - 1; j >= 0; j -= 1) {
            const m2jj = m2[n * j + j]
            if (salg.isZero(m2jj)) {
              throw Error('`m2` is singular.')
            }
            const nij = ni + j
            for (let k = j + 1; k < n; k += 1) {
              m1[nij] = salg.isub(m1[nij], salg.mul(m1[ni + k], m2[n * k + j]))
            }
            m1[nij] = salg.idiv(m1[nij], m2jj)
          }
        }
        const p = m2.permutation
        // ad hoc fix ...
        const flag = m1.isAdaptive()
        if (flag) {
          m1.setDim(0, dim2[0])
        }
        for (let i = p.length - 2; i >= 0; i -= 2) {
          m1.swapCol(p[i], p[i + 1])
        }
        if (flag) {
          m1.setDim(0)
        }
        // ... ad hoc fix
        return m1
      }
    } else {
      throw Error('neither `m1` nor `m2` is LU-factorised.')
    }
  }

  /**
   * @desc
   * The LinearAlgebra#det method calculates
   * the determinant of `m`, where `m` must be LU-factorised.
   *
   * @param {Matrix} m
   * a {@link Matrix}.
   *
   * @return {ScalarAlgebraicElement}
   * a {@link ScalarAlgebraicElement} representing
   * the determinant of `m`.
   *
   * @throws {Error}
   * if `m` is not a LU-factorised {@link Matrix}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let l = new LinearAlgebra(r)
   *
   * let m = l.$(5, 4, 5, 7).setDim(2, 2)
   *
   * l.det(m) // Error
   *
   * m = l.ilup(m)
   *
   * r.eq(l.det(m), 15) // true
   *
   * let m2 = l.$(0, 5, 7, 5).setDim(2, 2)
   * m2 = l.ilup(m2)
   * r.eq(l.det(m2), -35) // true
   */
  det (m) {
    m = this.cast(m)
    if (!m.isLUP()) {
      throw Error('`m` must be LU-factorised.')
    }
    const salg = this.salg
    const dim = m.getDim()[0]
    let prod = salg.num(1)
    for (let i = dim - 1; i >= 0; i -= 1) {
      prod = salg.imul(prod, m[i * dim + i])
    }
    if (m.permutation.length % 4 !== 0) {
      prod = salg.ineg(prod)
    }
    return prod
  }

  /**
   * @desc
   * The reviver function for the {@link LinearAlgebraicElement}s.
   *
   * @type {Function}
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
   * let str = JSON.stringify(m1)
   * let m2 = JSON.parse(str, l.reviver)
   * l.eq(m1, m2) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import { LinearAlgebra } from '@kkitahara/linear-algebra'
   * let r = new RealAlgebra()
   * let c = new ComplexAlgebra(r)
   * let l = new LinearAlgebra(c)
   *
   * let m1 = l.$(1, 2, 1, 2).setDim(2, 2)
   * m1 = l.ilup(m1)
   * let str = JSON.stringify(m1)
   * let m2 = JSON.parse(str, l.reviver)
   * m1.isLUP() // true
   * m2.isLUP() // true
   * // nullify just to use eq method
   * m1.permutation = null
   * m2.permutation = null
   * l.eq(m1, m2) // true
   */
  get reviver () {
    const salg = this.salg
    return function (key, value) {
      value = salg.reviver(key, value)
      value = Matrix.reviver(key, value)
      return value
    }
  }
}

/* @license-end */
