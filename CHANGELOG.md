v2.1.3
- Minor update (e.g. remove reduce methods)

v2.1.2
- Bug fix (LinearAlgebra#mmul for 0x0 matrix times 0x0 matrix)

v2.1.1
- Bug fix (LinearAlgebra#isolve)
- Change: Matrix#isAdaptive now returns true for matrices of length 0

v2.1.0
- Add 0 x 0 matrix support

v2.0.3
- Add CHANGELOG.md
