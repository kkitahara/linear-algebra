import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(4, 6)
let m2 = m1

// GOOD-PRACTICE!
m1 = l.ineg(m1)
testDriver.test(() => { return m1 === m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#ineg-example0_0', false)
testDriver.test(() => { return m1 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#ineg-example0_1', false)
testDriver.test(() => { return l.eq(m1, l.$(-4, -6)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ineg-example0_2', false)
