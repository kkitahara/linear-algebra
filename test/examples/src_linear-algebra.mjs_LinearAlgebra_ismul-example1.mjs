import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
let m2 = l.ilup(m1)
m2 = l.ismul(m2, 2)

testDriver.test(() => { return m2 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#ismul-example1_0', false)
testDriver.test(() => { return m2.isLUP() }, true, 'src/linear-algebra.mjs~LinearAlgebra#ismul-example1_1', false)
testDriver.test(() => { return r.eq(m2[0], 10) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ismul-example1_2', false)
testDriver.test(() => { return r.eq(m2[1], r.$(4, 5)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ismul-example1_3', false)
testDriver.test(() => { return r.eq(m2[2], 10) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ismul-example1_4', false)
testDriver.test(() => { return r.eq(m2[3], 6) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ismul-example1_5', false)
