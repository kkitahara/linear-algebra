import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '../../src/index.mjs'

let a = new M(1, 2, 3, 4, 5)
let b = a.slice()

testDriver.test(() => { return a.length === 5 }, true, 'src/matrix.mjs~Matrix.[Symbol.species]-example0_0', false)
testDriver.test(() => { return b.length === 5 }, true, 'src/matrix.mjs~Matrix.[Symbol.species]-example0_1', false)
testDriver.test(() => { return a === b }, false, 'src/matrix.mjs~Matrix.[Symbol.species]-example0_2', false)
testDriver.test(() => { return a.constructor === M }, true, 'src/matrix.mjs~Matrix.[Symbol.species]-example0_3', false)
testDriver.test(() => { return b.constructor === M }, false, 'src/matrix.mjs~Matrix.[Symbol.species]-example0_4', false)
testDriver.test(() => { return b.constructor === Array }, true, 'src/matrix.mjs~Matrix.[Symbol.species]-example0_5', false)
