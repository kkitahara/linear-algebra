import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(0, 2, 3, 0, 2, 3, 0, 2, 3).setDim(3, 3)
let m2 = l.ilup(m1)

testDriver.test(() => { return m1 === m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example3_0', false)
testDriver.test(() => { return m2 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example3_1', false)
testDriver.test(() => { return m2.permutation !== null }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example3_2', false)
testDriver.test(() => { return r.eq(m2[0], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example3_3', false)
testDriver.test(() => { return r.eq(m2[8], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example3_4', false)
