import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

testDriver.test(() => { return l.isExact() }, false, 'src/linear-algebra.mjs~LinearAlgebra#isExact-example1_0', false)
