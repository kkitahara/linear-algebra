import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
  from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m = l.$(1, -1)

let a = l.abs2(m)
testDriver.test(() => { return a instanceof M }, false, 'src/linear-algebra.mjs~LinearAlgebra#abs2-example0_0', false)
testDriver.test(() => { return a instanceof P }, true, 'src/linear-algebra.mjs~LinearAlgebra#abs2-example0_1', false)
testDriver.test(() => { return l.eq(m, l.$(1, -1)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#abs2-example0_2', false)
testDriver.test(() => { return r.eq(a, 2) }, true, 'src/linear-algebra.mjs~LinearAlgebra#abs2-example0_3', false)

m = l.$(5, 4, 5, 7).setDim(2, 2)
testDriver.test(() => { return l.abs2(l.lup(m)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#abs2-example0_4', false)
