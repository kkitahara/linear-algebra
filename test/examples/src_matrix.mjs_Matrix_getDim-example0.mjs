import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '../../src/index.mjs'

let a = new M(1, 2, 3, 4)

a.setDim(1, 4)
let dim = a.getDim()
testDriver.test(() => { return dim[0] }, 1, 'src/matrix.mjs~Matrix#getDim-example0_0', false)
testDriver.test(() => { return dim[1] }, 4, 'src/matrix.mjs~Matrix#getDim-example0_1', false)

a.setDim(2, 2)
dim = a.getDim()
testDriver.test(() => { return dim[0] }, 2, 'src/matrix.mjs~Matrix#getDim-example0_2', false)
testDriver.test(() => { return dim[1] }, 2, 'src/matrix.mjs~Matrix#getDim-example0_3', false)

a.setDim(0, 2)
dim = a.getDim()
testDriver.test(() => { return dim[0] }, 2, 'src/matrix.mjs~Matrix#getDim-example0_4', false)
testDriver.test(() => { return dim[1] }, 2, 'src/matrix.mjs~Matrix#getDim-example0_5', false)

a.setDim(0, 4)
dim = a.getDim()
testDriver.test(() => { return dim[0] }, 1, 'src/matrix.mjs~Matrix#getDim-example0_6', false)
testDriver.test(() => { return dim[1] }, 4, 'src/matrix.mjs~Matrix#getDim-example0_7', false)

a.setDim(1, 3)
testDriver.test(() => { return a.getDim() }, Error, 'src/matrix.mjs~Matrix#getDim-example0_8', false)

a.setDim(0, 5)
testDriver.test(() => { return a.getDim() }, Error, 'src/matrix.mjs~Matrix#getDim-example0_9', false)

a.setDim(3, 0)
testDriver.test(() => { return a.getDim() }, Error, 'src/matrix.mjs~Matrix#getDim-example0_10', false)

a = new M()
dim = a.getDim()
testDriver.test(() => { return dim[0] }, 0, 'src/matrix.mjs~Matrix#getDim-example0_11', false)
testDriver.test(() => { return dim[1] }, 0, 'src/matrix.mjs~Matrix#getDim-example0_12', false)
