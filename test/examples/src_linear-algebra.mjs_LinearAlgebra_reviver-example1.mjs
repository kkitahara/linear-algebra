import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '@kkitahara/complex-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let c = new ComplexAlgebra(r)
let l = new LinearAlgebra(c)

let m1 = l.$(1, 2, 1, 2).setDim(2, 2)
m1 = l.ilup(m1)
let str = JSON.stringify(m1)
let m2 = JSON.parse(str, l.reviver)
testDriver.test(() => { return m1.isLUP() }, true, 'src/linear-algebra.mjs~LinearAlgebra#reviver-example1_0', false)
testDriver.test(() => { return m2.isLUP() }, true, 'src/linear-algebra.mjs~LinearAlgebra#reviver-example1_1', false)
// nullify just to use eq method
m1.permutation = null
m2.permutation = null
testDriver.test(() => { return l.eq(m1, m2) }, true, 'src/linear-algebra.mjs~LinearAlgebra#reviver-example1_2', false)
