import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '@kkitahara/complex-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let c = new ComplexAlgebra(r)
let l = new LinearAlgebra(c)

let m1 = l.$(1, 2)
m1.setDim(2, 0)
let m2 = l.copy(m1)

testDriver.test(() => { return m1 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#copy-example0_0', false)
testDriver.test(() => { return m2 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#copy-example0_1', false)
testDriver.test(() => { return m1 !== m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#copy-example0_2', false)
testDriver.test(() => { return m1[0] !== m2[0] }, true, 'src/linear-algebra.mjs~LinearAlgebra#copy-example0_3', false)
testDriver.test(() => { return m1[1] !== m2[1] }, true, 'src/linear-algebra.mjs~LinearAlgebra#copy-example0_4', false)
testDriver.test(() => { return m1.nRow }, 2, 'src/linear-algebra.mjs~LinearAlgebra#copy-example0_5', false)
testDriver.test(() => { return m1.nCol }, 0, 'src/linear-algebra.mjs~LinearAlgebra#copy-example0_6', false)
testDriver.test(() => { return m2.nRow }, 2, 'src/linear-algebra.mjs~LinearAlgebra#copy-example0_7', false)
testDriver.test(() => { return m2.nCol }, 0, 'src/linear-algebra.mjs~LinearAlgebra#copy-example0_8', false)
testDriver.test(() => { return c.eq(m1[0], m2[0]) }, true, 'src/linear-algebra.mjs~LinearAlgebra#copy-example0_9', false)
testDriver.test(() => { return c.eq(m1[1], m2[1]) }, true, 'src/linear-algebra.mjs~LinearAlgebra#copy-example0_10', false)

let m3 = l.$(1, 2, 3, 4).setDim(2, 2)
m3 = l.ilup(m3)
let m4 = l.copy(m3)
testDriver.test(() => { return m4.isLUP() }, true, 'src/linear-algebra.mjs~LinearAlgebra#copy-example0_11', false)
