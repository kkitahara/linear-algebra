import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(1, 2, 1, 2).setDim(2, 2)
let m2 = l.lup(m1)
testDriver.test(() => { return l.solve(m2, m1) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#solve-example4_0', false)
testDriver.test(() => { return l.solve(m1, m2) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#solve-example4_1', false)
testDriver.test(() => { return l.solve(m2, l.$(1, 2, 1).setDim(1, 3)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#solve-example4_2', false)
testDriver.test(() => { return l.solve(l.$(1, 2, 1).setDim(1, 3), m2) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#solve-example4_3', false)
