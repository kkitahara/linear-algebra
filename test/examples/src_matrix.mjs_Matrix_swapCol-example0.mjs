import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '../../src/index.mjs'

let m1 = new M(1, 2, 3, 4).setDim(2, 2)
m1.swapCol(0, 1)

testDriver.test(() => { return m1[0] === 2 }, true, 'src/matrix.mjs~Matrix#swapCol-example0_0', false)
testDriver.test(() => { return m1[1] === 1 }, true, 'src/matrix.mjs~Matrix#swapCol-example0_1', false)
testDriver.test(() => { return m1[2] === 4 }, true, 'src/matrix.mjs~Matrix#swapCol-example0_2', false)
testDriver.test(() => { return m1[3] === 3 }, true, 'src/matrix.mjs~Matrix#swapCol-example0_3', false)

m1.swapCol(0, 0)
testDriver.test(() => { return m1[0] === 2 }, true, 'src/matrix.mjs~Matrix#swapCol-example0_4', false)
testDriver.test(() => { return m1[1] === 1 }, true, 'src/matrix.mjs~Matrix#swapCol-example0_5', false)
testDriver.test(() => { return m1[2] === 4 }, true, 'src/matrix.mjs~Matrix#swapCol-example0_6', false)
testDriver.test(() => { return m1[3] === 3 }, true, 'src/matrix.mjs~Matrix#swapCol-example0_7', false)

testDriver.test(() => { return m1.swapCol(0, -1) }, Error, 'src/matrix.mjs~Matrix#swapCol-example0_8', false)
testDriver.test(() => { return m1.swapCol(-1, 0) }, Error, 'src/matrix.mjs~Matrix#swapCol-example0_9', false)
