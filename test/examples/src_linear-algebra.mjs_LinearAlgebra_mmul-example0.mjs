import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '@kkitahara/complex-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let c = new ComplexAlgebra(r)
let l = new LinearAlgebra(c)

let m1 = l.$(1, c.$(0, 1), 3, 4).setDim(2, 0)
let m2 = l.$(1, 2, 3, 4).setDim(2, 0)
let m3 = l.mmul(m1, m2)
let m4 = l.$(c.$(1, 3), c.$(2, 4), 15, 22)
m4.setDim(2, 0)

testDriver.test(() => { return m3 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example0_0', false)
testDriver.test(() => { return l.eq(m1, l.$(1, c.$(0, 1), 3, 4).setDim(0, 2)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example0_1', false)
testDriver.test(() => { return l.eq(m2, l.$(1, 2, 3, 4).setDim(2, 0)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example0_2', false)
testDriver.test(() => { return l.eq(m3, m4) }, true, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example0_3', false)
testDriver.test(() => { return m3.nRow }, 2, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example0_4', false)
testDriver.test(() => { return m3.nCol }, 0, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example0_5', false)
