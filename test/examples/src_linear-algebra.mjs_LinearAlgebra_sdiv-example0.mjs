import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '@kkitahara/complex-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let c = new ComplexAlgebra(r)
let l = new LinearAlgebra(c)

let m1 = l.$(1, c.$(0, 1))
let m2 = l.sdiv(m1, c.$(0, 2))

testDriver.test(() => { return m2 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#sdiv-example0_0', false)
testDriver.test(() => { return m2 !== m1 }, true, 'src/linear-algebra.mjs~LinearAlgebra#sdiv-example0_1', false)
testDriver.test(() => { return l.eq(m1, l.$(1, c.$(0, 1))) }, true, 'src/linear-algebra.mjs~LinearAlgebra#sdiv-example0_2', false)
testDriver.test(() => { return l.eq(m2, l.$(c.$(0, r.$(-1, 2)), r.$(1, 2))) }, true, 'src/linear-algebra.mjs~LinearAlgebra#sdiv-example0_3', false)
