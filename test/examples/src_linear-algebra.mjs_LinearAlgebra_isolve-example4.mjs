import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(1, 2, 1, 2).setDim(2, 2)
let m2 = l.lup(m1)
testDriver.test(() => { return l.isolve(m2, m1) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example4_0', false)
testDriver.test(() => { return l.isolve(m1, m2) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example4_1', false)
