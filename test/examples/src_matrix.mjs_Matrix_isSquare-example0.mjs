import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '../../src/index.mjs'

let a = new M(1, 2, 3, 4)
testDriver.test(() => { return a.isSquare() }, Error, 'src/matrix.mjs~Matrix#isSquare-example0_0', false)

a.setDim(1, 4)
testDriver.test(() => { return a.isSquare() }, false, 'src/matrix.mjs~Matrix#isSquare-example0_1', false)

a.setDim(2, 2)
testDriver.test(() => { return a.isSquare() }, true, 'src/matrix.mjs~Matrix#isSquare-example0_2', false)
