import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(0, 3, 5, 4).setDim(2, 2)
let m2 = l.lup(m1)

testDriver.test(() => { return m1 !== m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example1_0', false)
testDriver.test(() => { return m2 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example1_1', false)
testDriver.test(() => { return m1.permutation === null }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example1_2', false)
testDriver.test(() => { return m2.permutation !== null }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example1_3', false)
testDriver.test(() => { return m2.permutation.length === 2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example1_4', false)
testDriver.test(() => { return m2.permutation[0] === 0 }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example1_5', false)
testDriver.test(() => { return m2.permutation[1] === 1 }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example1_6', false)
testDriver.test(() => { return r.eq(m2[0], 5) }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example1_7', false)
testDriver.test(() => { return r.eq(m2[1], r.$(4, 5)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example1_8', false)
testDriver.test(() => { return r.eq(m2[2], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example1_9', false)
testDriver.test(() => { return r.eq(m2[3], 3) }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example1_10', false)
