import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '@kkitahara/complex-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let c = new ComplexAlgebra(r)
let l = new LinearAlgebra(c)

let m1 = l.$(4, c.$(0, 6), 2)
let m2 = l.cjg(m1)

testDriver.test(() => { return m2 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#cjg-example0_0', false)
testDriver.test(() => { return m1 !== m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#cjg-example0_1', false)
testDriver.test(() => { return l.eq(m1, l.$(4, c.$(0, 6), 2)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#cjg-example0_2', false)
testDriver.test(() => { return l.eq(m2, l.$(4, c.$(0, -6), 2)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#cjg-example0_3', false)
