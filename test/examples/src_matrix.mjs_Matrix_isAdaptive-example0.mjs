import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '../../src/index.mjs'

let a = new M(1, 2, 3, 4)

testDriver.test(() => { return a.isAdaptive() }, true, 'src/matrix.mjs~Matrix#isAdaptive-example0_0', false)

a.setDim(1, 4)
testDriver.test(() => { return a.isAdaptive() }, false, 'src/matrix.mjs~Matrix#isAdaptive-example0_1', false)

a.setDim(0, 0)
testDriver.test(() => { return a.isAdaptive() }, true, 'src/matrix.mjs~Matrix#isAdaptive-example0_2', false)

a.setDim(0, 4)
testDriver.test(() => { return a.isAdaptive() }, false, 'src/matrix.mjs~Matrix#isAdaptive-example0_3', false)

a.setDim()
testDriver.test(() => { return a.isAdaptive() }, true, 'src/matrix.mjs~Matrix#isAdaptive-example0_4', false)

a = new M()
testDriver.test(() => { return a.isAdaptive() }, true, 'src/matrix.mjs~Matrix#isAdaptive-example0_5', false)
