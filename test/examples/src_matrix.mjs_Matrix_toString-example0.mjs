import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '../../src/index.mjs'

let a = new M(1, 2, 3, 4, 5, 6)
testDriver.test(() => { return a.toString() }, '(1, 2, 3, 4, 5, 6)', 'src/matrix.mjs~Matrix#toString-example0_0', false)

a.setDim(0, 2)
testDriver.test(() => { return a.toString() }, '(1, 2,\n 3, 4,\n 5, 6)', 'src/matrix.mjs~Matrix#toString-example0_1', false)

let b = new M(1, 5)
testDriver.test(() => { return b.toString(2) }, '(1, 101)', 'src/matrix.mjs~Matrix#toString-example0_2', false)

let c = new M()
testDriver.test(() => { return c.toString() }, '()', 'src/matrix.mjs~Matrix#toString-example0_3', false)
