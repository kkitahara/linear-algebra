import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
let m2 = l.lup(m1)
m1 = l.ilup(m1)
testDriver.test(() => { return l.isolve(m1, m2) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example3_0', false)
