import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
import bigInt from 'big-integer'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let a = r.$(bigInt('1e999'))

testDriver.test(() => { return l.isFinite(l.$(0)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isFinite-example1_0', false)
testDriver.test(() => { return l.isFinite(l.$(0, a, 1)) }, false, 'src/linear-algebra.mjs~LinearAlgebra#isFinite-example1_1', false)
