import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra(1e-5)
let l = new LinearAlgebra(r)

let m1 = l.$(1, 2, 1, 2).setDim(2, 2)
let m2 = l.ilup(m1)

testDriver.test(() => { return m1 === m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example4_0', false)
testDriver.test(() => { return m2 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example4_1', false)
testDriver.test(() => { return m2.permutation !== null }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example4_2', false)
testDriver.test(() => { return r.eq(m2[0], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example4_3', false)
