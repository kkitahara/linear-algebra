import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(1, 2, 3)
let m2 = l.$(1, 2, 3)
let m3 = l.$(3, 2, 1)
let m4 = l.$(1, 2)
let m5 = l.$(1, 2, 3, 4)

testDriver.test(() => { return l.ne(m1, m2) }, false, 'src/linear-algebra.mjs~LinearAlgebra#ne-example0_0', false)
testDriver.test(() => { return l.ne(m1, m3) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ne-example0_1', false)
testDriver.test(() => { return l.ne(m1, m4) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ne-example0_2', false)
testDriver.test(() => { return l.ne(m1, m5) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ne-example0_3', false)
