import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
let m2 = l.lup(m1)
let m3 = l.isolve(m1, m2)

testDriver.test(() => { return m3 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_0', false)
testDriver.test(() => { return m3 === m1 }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_1', false)
testDriver.test(() => { return m3 !== m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_2', false)
testDriver.test(() => { return m3.permutation === null }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_3', false)
testDriver.test(() => { return r.eq(m3[0], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_4', false)
testDriver.test(() => { return r.eq(m3[1], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_5', false)
testDriver.test(() => { return r.eq(m3[2], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_6', false)
testDriver.test(() => { return r.eq(m3[3], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_7', false)

let m4 = l.$(5, 4, 5, 7)
let m5 = l.isolve(m4, m2)
testDriver.test(() => { return m5 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_8', false)
testDriver.test(() => { return m5 === m4 }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_9', false)
testDriver.test(() => { return m5 !== m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_10', false)
testDriver.test(() => { return m5.permutation === null }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_11', false)
testDriver.test(() => { return r.eq(m5[0], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_12', false)
testDriver.test(() => { return r.eq(m5[1], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_13', false)
testDriver.test(() => { return r.eq(m5[2], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_14', false)
testDriver.test(() => { return r.eq(m5[3], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_15', false)

let m6 = l.$(5, 4, 5, 7)
let m7 = l.isolve(m2, m6)
testDriver.test(() => { return m7 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_16', false)
testDriver.test(() => { return m7 === m6 }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_17', false)
testDriver.test(() => { return m7 !== m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_18', false)
testDriver.test(() => { return m7.permutation === null }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_19', false)
testDriver.test(() => { return r.eq(m7[0], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_20', false)
testDriver.test(() => { return r.eq(m7[1], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_21', false)
testDriver.test(() => { return r.eq(m7[2], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_22', false)
testDriver.test(() => { return r.eq(m7[3], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example0_23', false)
