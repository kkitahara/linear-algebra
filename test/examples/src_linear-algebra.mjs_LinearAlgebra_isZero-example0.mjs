import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

testDriver.test(() => { return l.isZero(l.$(0, 0, 0, 0)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isZero-example0_0', false)
testDriver.test(() => { return l.isZero(l.$()) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isZero-example0_1', false)
testDriver.test(() => { return l.isZero(l.$(1, 2)) }, false, 'src/linear-algebra.mjs~LinearAlgebra#isZero-example0_2', false)

let m = l.ilup(l.$(0, 0, 0, 0).setDim(2, 2))
testDriver.test(() => { return l.isZero(m) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#isZero-example0_3', false)
