import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra, ComplexAlgebraicElement as C }
  from '@kkitahara/complex-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let c = new ComplexAlgebra(r)
let l = new LinearAlgebra(c)

let m1 = l.$(1, c.$(0, 1))
let m2 = l.$(1, c.$(0, 1))
let m3 = l.$(1, c.$(0, -1))

let z = l.dot(m1, m2)
testDriver.test(() => { return z instanceof M }, false, 'src/linear-algebra.mjs~LinearAlgebra#dot-example0_0', false)
testDriver.test(() => { return z instanceof C }, true, 'src/linear-algebra.mjs~LinearAlgebra#dot-example0_1', false)
testDriver.test(() => { return l.eq(m1, l.$(1, c.$(0, 1))) }, true, 'src/linear-algebra.mjs~LinearAlgebra#dot-example0_2', false)
testDriver.test(() => { return c.eq(z, 2) }, true, 'src/linear-algebra.mjs~LinearAlgebra#dot-example0_3', false)

let w = l.dot(m1, m3)
testDriver.test(() => { return w instanceof M }, false, 'src/linear-algebra.mjs~LinearAlgebra#dot-example0_4', false)
testDriver.test(() => { return w instanceof C }, true, 'src/linear-algebra.mjs~LinearAlgebra#dot-example0_5', false)
testDriver.test(() => { return c.eq(w, 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#dot-example0_6', false)

m1 = l.$(5, 4, 5, 7).setDim(2, 2)
m2 = l.$(1, 2, 3, 4).setDim(2, 2)
testDriver.test(() => { return l.dot(m1, l.lup(m2)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#dot-example0_7', false)
testDriver.test(() => { return l.dot(l.lup(m1), m2) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#dot-example0_8', false)
testDriver.test(() => { return l.dot(l.$(1, 2), l.$(1)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#dot-example0_9', false)
