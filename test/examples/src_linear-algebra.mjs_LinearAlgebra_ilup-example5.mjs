import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(1, 0, 0, 0, 1, 0, 0, 0, 1).setDim(3, 3)
let m2 = l.ilup(m1)

testDriver.test(() => { return m1 === m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example5_0', false)
testDriver.test(() => { return m2 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example5_1', false)
testDriver.test(() => { return m2.permutation !== null }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example5_2', false)
testDriver.test(() => { return m2.permutation.length === 0 }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example5_3', false)
testDriver.test(() => { return r.eq(m2[0], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example5_4', false)
testDriver.test(() => { return r.eq(m2[1], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example5_5', false)
testDriver.test(() => { return r.eq(m2[2], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example5_6', false)
testDriver.test(() => { return r.eq(m2[3], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example5_7', false)
testDriver.test(() => { return r.eq(m2[4], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example5_8', false)
testDriver.test(() => { return r.eq(m2[5], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example5_9', false)
testDriver.test(() => { return r.eq(m2[6], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example5_10', false)
testDriver.test(() => { return r.eq(m2[7], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example5_11', false)
testDriver.test(() => { return r.eq(m2[8], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example5_12', false)
