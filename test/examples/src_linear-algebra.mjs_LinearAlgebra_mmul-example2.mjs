import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '@kkitahara/complex-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let c = new ComplexAlgebra(r)
let l = new LinearAlgebra(c)

let m1 = l.$(1, c.$(0, 1))
let m2 = l.$(1, 2, 3, 4).setDim(2, 0)
let m3 = l.mmul(m1, m2)
let m4 = l.mmul(m2, m1)

testDriver.test(() => { return l.eq(m1, l.$(1, c.$(0, 1))) }, true, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example2_0', false)
testDriver.test(() => { return l.eq(m2, l.$(1, 2, 3, 4).setDim(2, 0)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example2_1', false)
testDriver.test(() => { return m3 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example2_2', false)
testDriver.test(() => { return m4 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example2_3', false)
testDriver.test(() => { return m3.isAdaptive() }, true, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example2_4', false)
testDriver.test(() => { return m4.isAdaptive() }, true, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example2_5', false)
testDriver.test(() => { return l.eq(m3, l.$(c.$(1, 3), c.$(2, 4))) }, true, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example2_6', false)
testDriver.test(() => { return l.eq(m4, l.$(c.$(1, 2), c.$(3, 4))) }, true, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example2_7', false)
