import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '../../src/index.mjs'

let a = new M(3)
testDriver.test(() => { return a instanceof M }, true, 'src/matrix.mjs~Matrix#constructor-example0_0', false)
testDriver.test(() => { return a.length === 1 }, true, 'src/matrix.mjs~Matrix#constructor-example0_1', false)
testDriver.test(() => { return a[0] === 3 }, true, 'src/matrix.mjs~Matrix#constructor-example0_2', false)
testDriver.test(() => { return a.nRow === 0 }, true, 'src/matrix.mjs~Matrix#constructor-example0_3', false)
testDriver.test(() => { return a.nCol === 0 }, true, 'src/matrix.mjs~Matrix#constructor-example0_4', false)

let b = new M(1, 2, 3)
testDriver.test(() => { return b instanceof M }, true, 'src/matrix.mjs~Matrix#constructor-example0_5', false)
testDriver.test(() => { return b.length === 3 }, true, 'src/matrix.mjs~Matrix#constructor-example0_6', false)
testDriver.test(() => { return b[0] === 1 }, true, 'src/matrix.mjs~Matrix#constructor-example0_7', false)
testDriver.test(() => { return b[1] === 2 }, true, 'src/matrix.mjs~Matrix#constructor-example0_8', false)
testDriver.test(() => { return b[2] === 3 }, true, 'src/matrix.mjs~Matrix#constructor-example0_9', false)
testDriver.test(() => { return b.nRow === 0 }, true, 'src/matrix.mjs~Matrix#constructor-example0_10', false)
testDriver.test(() => { return b.nCol === 0 }, true, 'src/matrix.mjs~Matrix#constructor-example0_11', false)
