import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(1, 2)
let m2 = l.$(3, 4)
let m3 = l.add(m1, m2)

testDriver.test(() => { return m3 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#add-example0_0', false)
testDriver.test(() => { return m1 !== m3 }, true, 'src/linear-algebra.mjs~LinearAlgebra#add-example0_1', false)
testDriver.test(() => { return l.eq(m1, l.$(1, 2)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#add-example0_2', false)
testDriver.test(() => { return l.eq(m3, l.$(4, 6)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#add-example0_3', false)
testDriver.test(() => { return l.add(m1, l.$(1, 2, 3)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#add-example0_4', false)
