import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '../../src/index.mjs'

let a = new M(1, 2, 3, 4, 5, 6).setDim(0, 3)

// toJSON method is called by JSON.stringify
let s = JSON.stringify(a)

testDriver.test(() => { return typeof s }, 'string', 'src/matrix.mjs~Matrix#toJSON-example0_0', false)
