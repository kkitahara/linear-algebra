import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
// import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)
let m1, m2, m3

// Generate a new matrix (since v2.0.0)
m1 = l.$(1, 0, 0, 1)
testDriver.test(() => { return m1.toString() }, '(1, 0, 0, 1)', 'src/linear-algebra.mjs~LinearAlgebra-example1_0', false)

m1 = l.$(r.$(1, 2, 5), 1, -1, 1)
testDriver.test(() => { return m1.toString() }, '((1 / 2)sqrt(5), 1, -1, 1)', 'src/linear-algebra.mjs~LinearAlgebra-example1_1', false)

// Some Array methods can be used
m1 = l.$(r.$(1, 2, 5), 1, -1, 1)
m1.push(r.$(3))
testDriver.test(() => { return m1.toString() }, '((1 / 2)sqrt(5), 1, -1, 1, 3)', 'src/linear-algebra.mjs~LinearAlgebra-example1_2', false)

// Set and get the dimension
m1 = l.$(1, 0, 0, 1)

// 2 x 2 matix
m1.setDim(2, 2)
// number of rows
testDriver.test(() => { return m1.getDim()[0] }, 2, 'src/linear-algebra.mjs~LinearAlgebra-example1_3', false)
// number of columns
testDriver.test(() => { return m1.getDim()[1] }, 2, 'src/linear-algebra.mjs~LinearAlgebra-example1_4', false)
// elements are stored in row-major order
testDriver.test(() => { return m1.toString() }, '(1, 0,\n 0, 1)', 'src/linear-algebra.mjs~LinearAlgebra-example1_5', false)

// 1 x 4 matix
m1.setDim(1, 4)
testDriver.test(() => { return m1.getDim()[0] }, 1, 'src/linear-algebra.mjs~LinearAlgebra-example1_6', false)
testDriver.test(() => { return m1.getDim()[1] }, 4, 'src/linear-algebra.mjs~LinearAlgebra-example1_7', false)
testDriver.test(() => { return m1.toString() }, '(1, 0, 0, 1)', 'src/linear-algebra.mjs~LinearAlgebra-example1_8', false)

// 4 x 1 matix
m1.setDim(4, 1)
testDriver.test(() => { return m1.getDim()[0] }, 4, 'src/linear-algebra.mjs~LinearAlgebra-example1_9', false)
testDriver.test(() => { return m1.getDim()[1] }, 1, 'src/linear-algebra.mjs~LinearAlgebra-example1_10', false)
testDriver.test(() => { return m1.toString() }, '(1,\n 0,\n 0,\n 1)', 'src/linear-algebra.mjs~LinearAlgebra-example1_11', false)

// 3 x 1 matix (inconsistent dimension)
m1.setDim(3, 1)
// throws an Error when getDim is called
testDriver.test(() => { return m1.getDim() }, Error, 'src/linear-algebra.mjs~LinearAlgebra-example1_12', false)

// 2 x 0 (0 means auto)
m1.setDim(2, 0)
testDriver.test(() => { return m1.getDim()[0] }, 2, 'src/linear-algebra.mjs~LinearAlgebra-example1_13', false)
testDriver.test(() => { return m1.getDim()[1] }, 2, 'src/linear-algebra.mjs~LinearAlgebra-example1_14', false)

// 0 x 1 (0 means auto)
m1.setDim(0, 1)
testDriver.test(() => { return m1.getDim()[0] }, 4, 'src/linear-algebra.mjs~LinearAlgebra-example1_15', false)
testDriver.test(() => { return m1.getDim()[1] }, 1, 'src/linear-algebra.mjs~LinearAlgebra-example1_16', false)

// 0 x 0 (this is an adaptive matrix, since v2.0.0)
m1.setDim(0, 0)
testDriver.test(() => { return m1.isAdaptive() }, true, 'src/linear-algebra.mjs~LinearAlgebra-example1_17', false)
testDriver.test(() => { return m1.getDim() }, Error, 'src/linear-algebra.mjs~LinearAlgebra-example1_18', false)

// matrices are adaptive by default
m1 = l.$(1, 0, 0, 1)
testDriver.test(() => { return m1.isAdaptive() }, true, 'src/linear-algebra.mjs~LinearAlgebra-example1_19', false)

// Copy (generate a new object)
m1 = l.$(1, 0, 0, 1).setDim(2, 2)
m2 = l.copy(m1)
testDriver.test(() => { return m2.toString() }, '(1, 0,\n 0, 1)', 'src/linear-algebra.mjs~LinearAlgebra-example1_20', false)
testDriver.test(() => { return m2.getDim()[0] }, 2, 'src/linear-algebra.mjs~LinearAlgebra-example1_21', false)
testDriver.test(() => { return m2.getDim()[1] }, 2, 'src/linear-algebra.mjs~LinearAlgebra-example1_22', false)

// Equality
m1 = l.$(1, 0, 0, 1).setDim(2, 2)
m2 = l.$(1, 0, 0, 1).setDim(2, 2)
m3 = l.$(1, 0, 0, -1).setDim(2, 2)
testDriver.test(() => { return l.eq(m1, m2) }, true, 'src/linear-algebra.mjs~LinearAlgebra-example1_23', false)
testDriver.test(() => { return l.eq(m1, m3) }, false, 'src/linear-algebra.mjs~LinearAlgebra-example1_24', false)

// matrices of different dimension are considered to be not equal
m1 = l.$(1, 0, 0, 1).setDim(2, 2)
m2 = l.$(1, 0, 0, 1).setDim(1, 4)
testDriver.test(() => { return l.eq(m1, m2) }, false, 'src/linear-algebra.mjs~LinearAlgebra-example1_25', false)

// here, `m1` is adaptive
m1 = l.$(1, 0, 0, 1)
m2 = l.$(1, 0, 0, 1).setDim(1, 4)
m3 = l.$(1, 0, 0, 1).setDim(4, 1)
testDriver.test(() => { return l.eq(m1, m2) }, true, 'src/linear-algebra.mjs~LinearAlgebra-example1_26', false)
testDriver.test(() => { return l.eq(m1, m3) }, true, 'src/linear-algebra.mjs~LinearAlgebra-example1_27', false)
testDriver.test(() => { return l.eq(m2, m3) }, false, 'src/linear-algebra.mjs~LinearAlgebra-example1_28', false)

// Inequality
m1 = l.$(1, 0, 0, 1).setDim(2, 2)
m2 = l.$(1, 0, 0, 1).setDim(2, 2)
m3 = l.$(1, 0, 0, -1).setDim(2, 2)
testDriver.test(() => { return l.ne(m1, m2) }, false, 'src/linear-algebra.mjs~LinearAlgebra-example1_29', false)
testDriver.test(() => { return l.ne(m1, m3) }, true, 'src/linear-algebra.mjs~LinearAlgebra-example1_30', false)

// matrices of different dimension are considered to be not equal
m1 = l.$(1, 0, 0, 1).setDim(2, 2)
m2 = l.$(1, 0, 0, 1).setDim(1, 4)
testDriver.test(() => { return l.ne(m1, m2) }, true, 'src/linear-algebra.mjs~LinearAlgebra-example1_31', false)

// here, `m1` is adaptive
m1 = l.$(1, 0, 0, 1)
m2 = l.$(1, 0, 0, 1).setDim(1, 4)
m3 = l.$(1, 0, 0, 1).setDim(4, 1)
testDriver.test(() => { return l.ne(m1, m2) }, false, 'src/linear-algebra.mjs~LinearAlgebra-example1_32', false)
testDriver.test(() => { return l.ne(m1, m3) }, false, 'src/linear-algebra.mjs~LinearAlgebra-example1_33', false)
testDriver.test(() => { return l.ne(m2, m3) }, true, 'src/linear-algebra.mjs~LinearAlgebra-example1_34', false)

// isZero
m1 = l.$(1, 0, 0, 1).setDim(2, 2)
m2 = l.$(0, 0, 0, 0).setDim(2, 2)
testDriver.test(() => { return l.isZero(m1) }, false, 'src/linear-algebra.mjs~LinearAlgebra-example1_35', false)
testDriver.test(() => { return l.isZero(m2) }, true, 'src/linear-algebra.mjs~LinearAlgebra-example1_36', false)

// Element-wise addition
m1 = l.$(1, 2, 3, 4)
m2 = l.$(1, 3, 1, 3)
// new object is generated
m3 = l.add(m1, m2)
testDriver.test(() => { return m3.toString() }, '(2, 5, 4, 7)', 'src/linear-algebra.mjs~LinearAlgebra-example1_37', false)

// In-place element-wise addition
m1 = l.$(1, 2, 3, 4)
m2 = l.$(1, 3, 1, 3)
// new object is not generated
m1 = l.iadd(m1, m2)
testDriver.test(() => { return m1.toString() }, '(2, 5, 4, 7)', 'src/linear-algebra.mjs~LinearAlgebra-example1_38', false)

// Element-wise subtraction
m1 = l.$(1, 2, 3, 4)
m2 = l.$(1, 3, 1, 3)
// new object is generated
m3 = l.sub(m1, m2)
testDriver.test(() => { return m3.toString() }, '(0, -1, 2, 1)', 'src/linear-algebra.mjs~LinearAlgebra-example1_39', false)

// In-place element-wise subtraction
m1 = l.$(1, 2, 3, 4)
m2 = l.$(1, 3, 1, 3)
// new object is not generated
m1 = l.isub(m1, m2)
testDriver.test(() => { return m1.toString() }, '(0, -1, 2, 1)', 'src/linear-algebra.mjs~LinearAlgebra-example1_40', false)

// Element-wise multiplication
m1 = l.$(1, 2, 3, 4)
m2 = l.$(1, 3, 1, 3)
// new object is generated
m3 = l.mul(m1, m2)
testDriver.test(() => { return m3.toString() }, '(1, 6, 3, 12)', 'src/linear-algebra.mjs~LinearAlgebra-example1_41', false)

// In-place element-wise multiplication
m1 = l.$(1, 2, 3, 4)
m2 = l.$(1, 3, 1, 3)
// new object is not generated
m1 = l.imul(m1, m2)
testDriver.test(() => { return m1.toString() }, '(1, 6, 3, 12)', 'src/linear-algebra.mjs~LinearAlgebra-example1_42', false)

// Element-wise division
m1 = l.$(1, 2, 3, 4)
m2 = l.$(1, 3, 1, 3)
// new object is generated
m3 = l.div(m1, m2)
testDriver.test(() => { return m3.toString() }, '(1, 2 / 3, 3, 4 / 3)', 'src/linear-algebra.mjs~LinearAlgebra-example1_43', false)

// In-place element-wise division
m1 = l.$(1, 2, 3, 4)
m2 = l.$(1, 3, 1, 3)
// new object is not generated
m1 = l.idiv(m1, m2)
testDriver.test(() => { return m1.toString() }, '(1, 2 / 3, 3, 4 / 3)', 'src/linear-algebra.mjs~LinearAlgebra-example1_44', false)

// Scalar multiplication
m1 = l.$(1, 2, 3, 4)
// new object is generated
m2 = l.smul(m1, r.$(1, 2))
testDriver.test(() => { return m2.toString() }, '(1 / 2, 1, 3 / 2, 2)', 'src/linear-algebra.mjs~LinearAlgebra-example1_45', false)

// In-place scalar multiplication
m1 = l.$(1, 2, 3, 4)
// new object is not generated
m1 = l.ismul(m1, r.$(1, 2))
testDriver.test(() => { return m1.toString() }, '(1 / 2, 1, 3 / 2, 2)', 'src/linear-algebra.mjs~LinearAlgebra-example1_46', false)

// Scalar multiplication by -1
m1 = l.$(1, 2, 3, 4)
// new object is generated
m2 = l.neg(m1)
testDriver.test(() => { return m2.toString() }, '(-1, -2, -3, -4)', 'src/linear-algebra.mjs~LinearAlgebra-example1_47', false)

// In-place scalar multiplication by -1
m1 = l.$(1, 2, 3, 4)
// new object is not generated
m1 = l.ineg(m1)
testDriver.test(() => { return m1.toString() }, '(-1, -2, -3, -4)', 'src/linear-algebra.mjs~LinearAlgebra-example1_48', false)

// Scalar division (since v2.0.0)
m1 = l.$(1, 2, 3, 4)
// new object is generated
m2 = l.sdiv(m1, 2)
testDriver.test(() => { return m2.toString() }, '(1 / 2, 1, 3 / 2, 2)', 'src/linear-algebra.mjs~LinearAlgebra-example1_49', false)

// In-place scalar division (since v2.0.0)
m1 = l.$(1, 2, 3, 4)
// new object is not generated
m1 = l.isdiv(m1, 2)
testDriver.test(() => { return m1.toString() }, '(1 / 2, 1, 3 / 2, 2)', 'src/linear-algebra.mjs~LinearAlgebra-example1_50', false)

// Complex conjugate
m1 = l.$(1, 2, 3, 4)
// new object is generated
m2 = l.cjg(m1)
testDriver.test(() => { return m2.toString() }, '(1, 2, 3, 4)', 'src/linear-algebra.mjs~LinearAlgebra-example1_51', false)

// In-place evaluation of the complex conjugate
m1 = l.$(1, 2, 3, 4)
// new object is not generated
m1 = l.icjg(m1)
testDriver.test(() => { return m1.toString() }, '(1, 2, 3, 4)', 'src/linear-algebra.mjs~LinearAlgebra-example1_52', false)

// Transpose
m1 = l.$(1, 2, 3, 4).setDim(2, 2)
// new object is generated
m2 = l.transpose(m1)
testDriver.test(() => { return m2.toString() }, '(1, 3,\n 2, 4)', 'src/linear-algebra.mjs~LinearAlgebra-example1_53', false)

// In-place evaluation of the transpose
m1 = l.$(1, 2, 3, 4).setDim(2, 2)
// new object is not generated
m1 = l.itranspose(m1)
testDriver.test(() => { return m1.toString() }, '(1, 3,\n 2, 4)', 'src/linear-algebra.mjs~LinearAlgebra-example1_54', false)

// Conjugate transpose (Hermitian transpose)
m1 = l.$(1, 2, 3, 4).setDim(2, 2)
// new object is generated
m2 = l.cjgTranspose(m1)
testDriver.test(() => { return m2.toString() }, '(1, 3,\n 2, 4)', 'src/linear-algebra.mjs~LinearAlgebra-example1_55', false)

// In-place evaluation of the conjugate transpose
m1 = l.$(1, 2, 3, 4).setDim(2, 2)
// new object is not generated
m1 = l.icjgTranspose(m1)
testDriver.test(() => { return m1.toString() }, '(1, 3,\n 2, 4)', 'src/linear-algebra.mjs~LinearAlgebra-example1_56', false)

// Dot product
m1 = l.$(1, 2)
m2 = l.$(3, 2)
testDriver.test(() => { return l.dot(m1, m2).toString() }, '7', 'src/linear-algebra.mjs~LinearAlgebra-example1_57', false)

// Square of the absolute value (Frobenius norm)
m1 = l.$(1, 2)
let a = l.abs2(m1)
testDriver.test(() => { return a.toString() }, '5', 'src/linear-algebra.mjs~LinearAlgebra-example1_58', false)

// Matrix multiplication
m1 = l.$(1, 2, 3, 4).setDim(2, 2)
m2 = l.$(1, 3, 1, 3).setDim(2, 2)
m3 = l.mmul(m1, m2)
testDriver.test(() => { return m3.toString() }, '(3, 9,\n 7, 21)', 'src/linear-algebra.mjs~LinearAlgebra-example1_59', false)

// LU-factorisation
m1 = l.$(1, 2, 3, 4).setDim(2, 2)
// new object is generated
m2 = l.lup(m1)

// In-place LU-factorisation
m1 = l.$(1, 2, 3, 4).setDim(2, 2)
// new object is not generated
m1 = l.ilup(m1)

// Solving a linear equation
m1 = l.$(1, 2, 3, 4).setDim(2, 2)
m2 = l.$(1, 2, 3, 4).setDim(2, 2)

// m1 m3 = m2, new object is generated
m3 = l.solve(l.lup(m1), m2)
testDriver.test(() => { return m3.toString() }, '(1, 0,\n 0, 1)', 'src/linear-algebra.mjs~LinearAlgebra-example1_60', false)

// m3 m1 = m2, new object is generated
m3 = l.solve(m2, l.lup(m1))
testDriver.test(() => { return m3.toString() }, '(1, 0,\n 0, 1)', 'src/linear-algebra.mjs~LinearAlgebra-example1_61', false)

// Solving a linear equation in-place
m1 = l.$(1, 2, 3, 4).setDim(2, 2)
m2 = l.$(1, 2, 3, 4).setDim(2, 2)

// m1 m3 = m2, new object is not generated
m2 = l.solve(l.lup(m1), m2)
testDriver.test(() => { return m2.toString() }, '(1, 0,\n 0, 1)', 'src/linear-algebra.mjs~LinearAlgebra-example1_62', false)

m2 = l.$(1, 2, 3, 4).setDim(2, 2)

// m3 m1 = m2, new object is not generated
m2 = l.solve(m2, l.lup(m1))
testDriver.test(() => { return m2.toString() }, '(1, 0,\n 0, 1)', 'src/linear-algebra.mjs~LinearAlgebra-example1_63', false)

// Determinant
m1 = l.$(1, 2, 3, 4).setDim(2, 2)
m2 = l.lup(m1)
// det method supports only LU-factorised matrices
let det = l.det(m2)
testDriver.test(() => { return det.toString() }, '-2', 'src/linear-algebra.mjs~LinearAlgebra-example1_64', false)

// JSON (stringify and parse)
m1 = l.$(1, 2, 3, 4).setDim(2, 2)
let str = JSON.stringify(m1)
m2 = JSON.parse(str, l.reviver)
testDriver.test(() => { return l.eq(m1, m2) }, true, 'src/linear-algebra.mjs~LinearAlgebra-example1_65', false)
