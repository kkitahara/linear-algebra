import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra(1e-5)
let l = new LinearAlgebra(r)

let m1 = l.$(0, 3, 5, 4).setDim(2, 2)
let m2 = l.ilup(m1)
let m3 = l.ilup(m2)

testDriver.test(() => { return m2 === m3 }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example2_0', false)

testDriver.test(() => { return m1 === m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example2_1', false)
testDriver.test(() => { return m2 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example2_2', false)
testDriver.test(() => { return m2.permutation !== null }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example2_3', false)
testDriver.test(() => { return m2.permutation.length === 2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example2_4', false)
testDriver.test(() => { return m2.permutation[0] === 0 }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example2_5', false)
testDriver.test(() => { return m2.permutation[1] === 1 }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example2_6', false)
testDriver.test(() => { return r.eq(m2[0], 5) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example2_7', false)
testDriver.test(() => { return r.eq(m2[1], r.$(4, 5)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example2_8', false)
testDriver.test(() => { return r.eq(m2[2], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example2_9', false)
testDriver.test(() => { return r.eq(m2[3], 3) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ilup-example2_10', false)
