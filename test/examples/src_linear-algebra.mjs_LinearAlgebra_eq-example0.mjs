import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(1, 2, 3)
let m2 = l.$(1, 2, 3)
let m3 = l.$(3, 2, 1)
let m4 = l.$(1, 2)
let m5 = l.$(1, 2, 3, 4)

testDriver.test(() => { return l.eq(m1, m2) }, true, 'src/linear-algebra.mjs~LinearAlgebra#eq-example0_0', false)
testDriver.test(() => { return l.eq(m1, m3) }, false, 'src/linear-algebra.mjs~LinearAlgebra#eq-example0_1', false)
testDriver.test(() => { return l.eq(m1, m4) }, false, 'src/linear-algebra.mjs~LinearAlgebra#eq-example0_2', false)
testDriver.test(() => { return l.eq(m1, m5) }, false, 'src/linear-algebra.mjs~LinearAlgebra#eq-example0_3', false)

m1.setDim(1, 3)
m2.setDim(3, 1)
testDriver.test(() => { return l.eq(m1, m2) }, false, 'src/linear-algebra.mjs~LinearAlgebra#eq-example0_4', false)

m1 = l.$(1, 2, 3, 4).setDim(2, 2)
m2 = l.$(1, 2, 3, 4).setDim(2, 2)
testDriver.test(() => { return l.eq(l.lup(m1), m2) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#eq-example0_5', false)
testDriver.test(() => { return l.eq(m1, l.lup(m2)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#eq-example0_6', false)
