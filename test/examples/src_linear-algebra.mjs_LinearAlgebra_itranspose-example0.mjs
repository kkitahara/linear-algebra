import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '@kkitahara/complex-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let c = new ComplexAlgebra(r)
let l = new LinearAlgebra(c)

let m1 = l.$(4, c.$(0, 6), 2, 3).setDim(2, 2)
let m2 = m1

// GOOD-PRACTICE!
m1 = l.itranspose(m1)
testDriver.test(() => { return m1 === m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#itranspose-example0_0', false)
testDriver.test(() => { return m1 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#itranspose-example0_1', false)
testDriver.test(() => { return l.eq(m1, l.$(4, 2, c.$(0, 6), 3).setDim(2, 2)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#itranspose-example0_2', false)

m1 = l.$(5, 4, 5, 7).setDim(2, 2)
testDriver.test(() => { return l.itranspose(l.lup(m1)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#itranspose-example0_3', false)
testDriver.test(() => { return l.itranspose(l.$(5, 4, 5, 7)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#itranspose-example0_4', false)
