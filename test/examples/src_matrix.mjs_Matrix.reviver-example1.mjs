import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '../../src/index.mjs'

let a = new M(1, 2, 3, 4)
a.permutation = [1, 2, 3, 4]
let s = JSON.stringify(a)

// reviver method is called by JSON.parse
let b = JSON.parse(s, M.reviver)

testDriver.test(() => { return typeof s }, 'string', 'src/matrix.mjs~Matrix.reviver-example1_0', false)
testDriver.test(() => { return a === b }, false, 'src/matrix.mjs~Matrix.reviver-example1_1', false)
testDriver.test(() => { return a.length === b.length }, true, 'src/matrix.mjs~Matrix.reviver-example1_2', false)
testDriver.test(() => { return a.nRow === b.nRow }, true, 'src/matrix.mjs~Matrix.reviver-example1_3', false)
testDriver.test(() => { return a.nCol === b.nRow }, true, 'src/matrix.mjs~Matrix.reviver-example1_4', false)
testDriver.test(() => { return a[0] === b[0] }, true, 'src/matrix.mjs~Matrix.reviver-example1_5', false)
testDriver.test(() => { return a[1] === b[1] }, true, 'src/matrix.mjs~Matrix.reviver-example1_6', false)
testDriver.test(() => { return a[2] === b[2] }, true, 'src/matrix.mjs~Matrix.reviver-example1_7', false)
testDriver.test(() => { return a[3] === b[3] }, true, 'src/matrix.mjs~Matrix.reviver-example1_8', false)
testDriver.test(() => { return b.permutation[0] === 1 }, true, 'src/matrix.mjs~Matrix.reviver-example1_9', false)
testDriver.test(() => { return b.permutation[1] === 2 }, true, 'src/matrix.mjs~Matrix.reviver-example1_10', false)
testDriver.test(() => { return b.permutation[2] === 3 }, true, 'src/matrix.mjs~Matrix.reviver-example1_11', false)
testDriver.test(() => { return b.permutation[3] === 4 }, true, 'src/matrix.mjs~Matrix.reviver-example1_12', false)
