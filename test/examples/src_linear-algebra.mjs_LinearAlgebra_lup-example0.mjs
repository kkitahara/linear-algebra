import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
let m2 = l.lup(m1)

testDriver.test(() => { return m1 !== m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example0_0', false)
testDriver.test(() => { return m2 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example0_1', false)
testDriver.test(() => { return m1.permutation === null }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example0_2', false)
testDriver.test(() => { return m2.permutation !== null }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example0_3', false)
testDriver.test(() => { return m2.permutation.length === 0 }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example0_4', false)
testDriver.test(() => { return r.eq(m2[0], 5) }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example0_5', false)
testDriver.test(() => { return r.eq(m2[1], r.$(4, 5)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example0_6', false)
testDriver.test(() => { return r.eq(m2[2], 5) }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example0_7', false)
testDriver.test(() => { return r.eq(m2[3], 3) }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example0_8', false)

testDriver.test(() => { return l.lup(l.$(5, 4, 5, 7)).getDim()[0] }, 2, 'src/linear-algebra.mjs~LinearAlgebra#lup-example0_9', false)
testDriver.test(() => { return l.lup(l.$(5, 4, 5, 7).setDim(1, 4)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#lup-example0_10', false)
