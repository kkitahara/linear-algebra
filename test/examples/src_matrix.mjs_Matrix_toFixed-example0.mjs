import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '../../src/index.mjs'

let a = new M(1, 2, 3, 4, 5, 6)
testDriver.test(() => { return a.toFixed(3) }, '(1.000, 2.000, 3.000, 4.000, 5.000, 6.000)', 'src/matrix.mjs~Matrix#toFixed-example0_0', false)

a.setDim(0, 2)
testDriver.test(() => { return a.toFixed(3) }, '(1.000, 2.000,\n 3.000, 4.000,\n 5.000, 6.000)', 'src/matrix.mjs~Matrix#toFixed-example0_1', false)

let b = new M()
testDriver.test(() => { return b.toFixed(3) }, '()', 'src/matrix.mjs~Matrix#toFixed-example0_2', false)
