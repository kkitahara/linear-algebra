import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m = l.$(5, 4, 5, 7).setDim(2, 2)

testDriver.test(() => { return l.det(m) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#det-example0_0', false)

m = l.ilup(m)

testDriver.test(() => { return r.eq(l.det(m), 15) }, true, 'src/linear-algebra.mjs~LinearAlgebra#det-example0_1', false)

let m2 = l.$(0, 5, 7, 5).setDim(2, 2)
m2 = l.ilup(m2)
testDriver.test(() => { return r.eq(l.det(m2), -35) }, true, 'src/linear-algebra.mjs~LinearAlgebra#det-example0_2', false)
