import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '../../src/index.mjs'

let a = new M(1, 2, 3, 4).setDim(2, 2)
testDriver.test(() => { return a.nRow === 2 }, true, 'src/matrix.mjs~Matrix#setDim-example0_0', false)
testDriver.test(() => { return a.nCol === 2 }, true, 'src/matrix.mjs~Matrix#setDim-example0_1', false)

a.setDim(1, 2)
testDriver.test(() => { return a.nRow === 1 }, true, 'src/matrix.mjs~Matrix#setDim-example0_2', false)
testDriver.test(() => { return a.nCol === 2 }, true, 'src/matrix.mjs~Matrix#setDim-example0_3', false)

testDriver.test(() => { return a.setDim(-2, 2) }, Error, 'src/matrix.mjs~Matrix#setDim-example0_4', false)
testDriver.test(() => { return a.setDim(2, -2) }, Error, 'src/matrix.mjs~Matrix#setDim-example0_5', false)
testDriver.test(() => { return a.setDim(null, 2) }, Error, 'src/matrix.mjs~Matrix#setDim-example0_6', false)
testDriver.test(() => { return a.setDim(2, null) }, Error, 'src/matrix.mjs~Matrix#setDim-example0_7', false)
testDriver.test(() => { return a.setDim(2.1, 2) }, Error, 'src/matrix.mjs~Matrix#setDim-example0_8', false)
testDriver.test(() => { return a.setDim(2, 2.1) }, Error, 'src/matrix.mjs~Matrix#setDim-example0_9', false)
