import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
let m2 = l.ilup(m1)
let m3 = l.sdiv(m2, r.$(1, 2))

testDriver.test(() => { return m3 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#sdiv-example1_0', false)
testDriver.test(() => { return m3.isLUP() }, true, 'src/linear-algebra.mjs~LinearAlgebra#sdiv-example1_1', false)
testDriver.test(() => { return r.eq(m3[0], 10) }, true, 'src/linear-algebra.mjs~LinearAlgebra#sdiv-example1_2', false)
testDriver.test(() => { return r.eq(m3[1], r.$(4, 5)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#sdiv-example1_3', false)
testDriver.test(() => { return r.eq(m3[2], 10) }, true, 'src/linear-algebra.mjs~LinearAlgebra#sdiv-example1_4', false)
testDriver.test(() => { return r.eq(m3[3], 6) }, true, 'src/linear-algebra.mjs~LinearAlgebra#sdiv-example1_5', false)
