import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(1, 2, 1, 2).setDim(2, 2)
let m2 = l.lup(m1)

testDriver.test(() => { return m1 !== m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example2_0', false)
testDriver.test(() => { return m2 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example2_1', false)
testDriver.test(() => { return m2.permutation !== null }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example2_2', false)
testDriver.test(() => { return r.eq(m2[0], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example2_3', false)
testDriver.test(() => { return r.eq(m2[3], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example2_4', false)

let m3 = l.lup(m2)
testDriver.test(() => { return m1 !== m3 }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example2_5', false)
testDriver.test(() => { return m2 !== m3 }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example2_6', false)
testDriver.test(() => { return m3 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example2_7', false)
testDriver.test(() => { return m3.permutation !== null }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example2_8', false)
testDriver.test(() => { return r.eq(m3[0], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example2_9', false)
testDriver.test(() => { return r.eq(m3[3], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#lup-example2_10', false)

testDriver.test(() => { return l.lup(l.$(1, 2, 3)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#lup-example2_11', false)
