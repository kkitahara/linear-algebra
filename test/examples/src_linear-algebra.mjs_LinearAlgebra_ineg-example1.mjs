import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
let m2 = l.ilup(m1)
m2 = l.ineg(m2)

testDriver.test(() => { return m2 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#ineg-example1_0', false)
testDriver.test(() => { return m2.isLUP() }, true, 'src/linear-algebra.mjs~LinearAlgebra#ineg-example1_1', false)
testDriver.test(() => { return r.eq(m2[0], -5) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ineg-example1_2', false)
testDriver.test(() => { return r.eq(m2[1], r.$(4, 5)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ineg-example1_3', false)
testDriver.test(() => { return r.eq(m2[2], -5) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ineg-example1_4', false)
testDriver.test(() => { return r.eq(m2[3], -3) }, true, 'src/linear-algebra.mjs~LinearAlgebra#ineg-example1_5', false)
