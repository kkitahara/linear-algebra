import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(0, 4, 5, 7).setDim(2, 2)
let m2 = l.lup(m1)
let m3 = l.isolve(m1, m2)

testDriver.test(() => { return m3 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_0', false)
testDriver.test(() => { return m3 === m1 }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_1', false)
testDriver.test(() => { return m3 !== m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_2', false)
testDriver.test(() => { return m3.permutation === null }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_3', false)
testDriver.test(() => { return r.eq(m3[0], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_4', false)
testDriver.test(() => { return r.eq(m3[1], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_5', false)
testDriver.test(() => { return r.eq(m3[2], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_6', false)
testDriver.test(() => { return r.eq(m3[3], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_7', false)

m1 = l.$(0, 4, 5, 7).setDim(2, 2)
m3 = l.isolve(m2, m1)

testDriver.test(() => { return m3 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_8', false)
testDriver.test(() => { return m3 === m1 }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_9', false)
testDriver.test(() => { return m3 !== m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_10', false)
testDriver.test(() => { return m3.permutation === null }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_11', false)
testDriver.test(() => { return r.eq(m3[0], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_12', false)
testDriver.test(() => { return r.eq(m3[1], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_13', false)
testDriver.test(() => { return r.eq(m3[2], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_14', false)
testDriver.test(() => { return r.eq(m3[3], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_15', false)

testDriver.test(() => { return l.isolve(l.$(0, 4, 5).setDim(1, 3), m2) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_16', false)
testDriver.test(() => { return l.isolve(m2, l.$(0, 4, 5).setDim(1, 3)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#isolve-example1_17', false)
