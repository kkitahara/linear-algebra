import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
let m2 = l.lup(m1)
let m3 = l.solve(m2, m1)

testDriver.test(() => { return m3 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#solve-example1_0', false)
testDriver.test(() => { return m3 !== m1 }, true, 'src/linear-algebra.mjs~LinearAlgebra#solve-example1_1', false)
testDriver.test(() => { return m3 !== m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#solve-example1_2', false)
testDriver.test(() => { return m3.permutation === null }, true, 'src/linear-algebra.mjs~LinearAlgebra#solve-example1_3', false)
testDriver.test(() => { return r.eq(m3[0], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#solve-example1_4', false)
testDriver.test(() => { return r.eq(m3[1], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#solve-example1_5', false)
testDriver.test(() => { return r.eq(m3[2], 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#solve-example1_6', false)
testDriver.test(() => { return r.eq(m3[3], 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#solve-example1_7', false)
