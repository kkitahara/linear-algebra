import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

testDriver.test(() => { return l.isInteger([0, 0, 0, 0]) }, true, 'src/linear-algebra.mjs~LinearAlgebra#isInteger-example0_0', false)
testDriver.test(() => { return l.isInteger([1, 3, -2, r.$(1, 2)]) }, false, 'src/linear-algebra.mjs~LinearAlgebra#isInteger-example0_1', false)

let m = l.ilup(l.$(0, 0, 0, 0).setDim(2, 2))
testDriver.test(() => { return l.isInteger(m) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#isInteger-example0_2', false)
