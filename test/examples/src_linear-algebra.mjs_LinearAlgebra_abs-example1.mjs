import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m = new M(1, -1)

let a = l.abs(m)
testDriver.test(() => { return a instanceof M }, false, 'src/linear-algebra.mjs~LinearAlgebra#abs-example1_0', false)
testDriver.test(() => { return typeof a === 'number' }, true, 'src/linear-algebra.mjs~LinearAlgebra#abs-example1_1', false)
testDriver.test(() => { return l.eq(m, new M(1, -1)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#abs-example1_2', false)
testDriver.test(() => { return r.eq(a, Math.sqrt(2)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#abs-example1_3', false)

m = new M(5, 4, 5, 7).setDim(2, 2)
testDriver.test(() => { return l.abs(l.lup(m)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#abs-example1_4', false)
