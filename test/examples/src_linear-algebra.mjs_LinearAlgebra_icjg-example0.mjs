import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '@kkitahara/complex-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let c = new ComplexAlgebra(r)
let l = new LinearAlgebra(c)

let m1 = l.$(4, c.$(0, 6), 2)
let m2 = m1

// GOOD-PRACTICE!
m1 = l.icjg(m1)
testDriver.test(() => { return m1 === m2 }, true, 'src/linear-algebra.mjs~LinearAlgebra#icjg-example0_0', false)
testDriver.test(() => { return m1 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#icjg-example0_1', false)
testDriver.test(() => { return l.eq(m1, l.$(4, c.$(0, -6), 2)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#icjg-example0_2', false)
