import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(1, 2)
let m2 = l.$(3, 4)
let m3 = m1

// GOOD-PRACTICE!
m1 = l.imul(m1, m2)
testDriver.test(() => { return m1 === m3 }, true, 'src/linear-algebra.mjs~LinearAlgebra#imul-example0_0', false)
testDriver.test(() => { return m1 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#imul-example0_1', false)
testDriver.test(() => { return l.eq(m1, l.$(3, 8)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#imul-example0_2', false)

m1 = l.$(1, 2, 3, 4).setDim(2, 2)
m2 = l.$(5, 7, 5, 3).setDim(2, 2)
testDriver.test(() => { return l.imul(l.lup(m1), m2) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#imul-example0_3', false)
testDriver.test(() => { return l.imul(m1, l.lup(m2)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#imul-example0_4', false)
testDriver.test(() => { return l.imul(m1, l.$(1, 2, 3)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#imul-example0_5', false)
