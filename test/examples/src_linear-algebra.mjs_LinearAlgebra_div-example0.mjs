import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m1 = l.$(4, 6)
let m2 = l.$(2, 2)
let m3 = l.div(m1, m2)

testDriver.test(() => { return m3 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#div-example0_0', false)
testDriver.test(() => { return m1 !== m3 }, true, 'src/linear-algebra.mjs~LinearAlgebra#div-example0_1', false)
testDriver.test(() => { return l.eq(m1, l.$(4, 6)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#div-example0_2', false)
testDriver.test(() => { return l.eq(m3, l.$(2, 3)) }, true, 'src/linear-algebra.mjs~LinearAlgebra#div-example0_3', false)
testDriver.test(() => { return l.div(m1, l.$(1, 2, 3)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#div-example0_4', false)
