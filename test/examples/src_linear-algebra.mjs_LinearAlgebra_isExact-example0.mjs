import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

testDriver.test(() => { return l.isExact() }, true, 'src/linear-algebra.mjs~LinearAlgebra#isExact-example0_0', false)
