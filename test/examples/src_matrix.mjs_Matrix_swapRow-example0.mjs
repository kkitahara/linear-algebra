import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '../../src/index.mjs'

let m1 = new M(1, 2, 3, 4).setDim(2, 2)
m1.swapRow(0, 1)

testDriver.test(() => { return m1[0] === 3 }, true, 'src/matrix.mjs~Matrix#swapRow-example0_0', false)
testDriver.test(() => { return m1[1] === 4 }, true, 'src/matrix.mjs~Matrix#swapRow-example0_1', false)
testDriver.test(() => { return m1[2] === 1 }, true, 'src/matrix.mjs~Matrix#swapRow-example0_2', false)
testDriver.test(() => { return m1[3] === 2 }, true, 'src/matrix.mjs~Matrix#swapRow-example0_3', false)

m1.swapRow(0, 0)
testDriver.test(() => { return m1[0] === 3 }, true, 'src/matrix.mjs~Matrix#swapRow-example0_4', false)
testDriver.test(() => { return m1[1] === 4 }, true, 'src/matrix.mjs~Matrix#swapRow-example0_5', false)
testDriver.test(() => { return m1[2] === 1 }, true, 'src/matrix.mjs~Matrix#swapRow-example0_6', false)
testDriver.test(() => { return m1[3] === 2 }, true, 'src/matrix.mjs~Matrix#swapRow-example0_7', false)

testDriver.test(() => { return m1.swapRow(0, -1) }, Error, 'src/matrix.mjs~Matrix#swapRow-example0_8', false)
testDriver.test(() => { return m1.swapRow(-1, 0) }, Error, 'src/matrix.mjs~Matrix#swapRow-example0_9', false)
