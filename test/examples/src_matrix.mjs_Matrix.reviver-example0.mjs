import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '../../src/index.mjs'

let a = new M(1, 2, 3, 4)
let s = JSON.stringify(a)

// reviver method is called by JSON.parse
let b = JSON.parse(s, M.reviver)

testDriver.test(() => { return typeof s }, 'string', 'src/matrix.mjs~Matrix.reviver-example0_0', false)
testDriver.test(() => { return a === b }, false, 'src/matrix.mjs~Matrix.reviver-example0_1', false)
testDriver.test(() => { return a.length === b.length }, true, 'src/matrix.mjs~Matrix.reviver-example0_2', false)
testDriver.test(() => { return a.nRow === b.nRow }, true, 'src/matrix.mjs~Matrix.reviver-example0_3', false)
testDriver.test(() => { return a.nCol === b.nRow }, true, 'src/matrix.mjs~Matrix.reviver-example0_4', false)
testDriver.test(() => { return a[0] === b[0] }, true, 'src/matrix.mjs~Matrix.reviver-example0_5', false)
testDriver.test(() => { return a[1] === b[1] }, true, 'src/matrix.mjs~Matrix.reviver-example0_6', false)
testDriver.test(() => { return a[2] === b[2] }, true, 'src/matrix.mjs~Matrix.reviver-example0_7', false)
testDriver.test(() => { return a[3] === b[3] }, true, 'src/matrix.mjs~Matrix.reviver-example0_8', false)

let s2 = s.replace('2.0.0', '1.0.0')
b = JSON.parse(s2, M.reviver)
testDriver.test(() => { return typeof s2 }, 'string', 'src/matrix.mjs~Matrix.reviver-example0_9', false)
testDriver.test(() => { return a === b }, false, 'src/matrix.mjs~Matrix.reviver-example0_10', false)
testDriver.test(() => { return a.length === b.length }, true, 'src/matrix.mjs~Matrix.reviver-example0_11', false)
testDriver.test(() => { return a.nRow === b.nRow }, true, 'src/matrix.mjs~Matrix.reviver-example0_12', false)
testDriver.test(() => { return a.nCol === b.nRow }, true, 'src/matrix.mjs~Matrix.reviver-example0_13', false)
testDriver.test(() => { return a[0] === b[0] }, true, 'src/matrix.mjs~Matrix.reviver-example0_14', false)
testDriver.test(() => { return a[1] === b[1] }, true, 'src/matrix.mjs~Matrix.reviver-example0_15', false)
testDriver.test(() => { return a[2] === b[2] }, true, 'src/matrix.mjs~Matrix.reviver-example0_16', false)
testDriver.test(() => { return a[3] === b[3] }, true, 'src/matrix.mjs~Matrix.reviver-example0_17', false)

let s3 = s.replace('2.0.0', '0.0.0')
testDriver.test(() => { return JSON.parse(s3, M.reviver) }, Error, 'src/matrix.mjs~Matrix.reviver-example0_18', false)
