import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

let m = l.$(1, -1)

testDriver.test(() => { return l.abs(m) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#abs-example0_0', false)
