import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '@kkitahara/complex-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let c = new ComplexAlgebra(r)
let l = new LinearAlgebra(c)

let m1 = l.$(5, 4, 5, 7).setDim(2, 2)
let str = JSON.stringify(m1)
let m2 = JSON.parse(str, l.reviver)
testDriver.test(() => { return l.eq(m1, m2) }, true, 'src/linear-algebra.mjs~LinearAlgebra#reviver-example0_0', false)
