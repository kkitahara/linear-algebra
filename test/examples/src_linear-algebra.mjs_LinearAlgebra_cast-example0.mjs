import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
  from '@kkitahara/real-algebra'
import { ComplexAlgebra, ComplexAlgebraicElement as C }
  from '@kkitahara/complex-algebra'
import { LinearAlgebra, Matrix as M } from '../../src/index.mjs'
let r = new RealAlgebra()
let c = new ComplexAlgebra(r)
let l = new LinearAlgebra(c)

let m1 = l.$(c.$(1))
let m2 = m1

testDriver.test(() => { return m2 === l.cast(m1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_0', false)
testDriver.test(() => { return m1[0] instanceof C }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_1', false)
testDriver.test(() => { return m1[0].re instanceof P }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_2', false)
testDriver.test(() => { return r.eq(m1[0].re, 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_3', false)
testDriver.test(() => { return m1[0].im instanceof P }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_4', false)
testDriver.test(() => { return r.eq(m1[0].im, 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_5', false)

let m3 = l.cast([1, 2])
testDriver.test(() => { return m3 instanceof M }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_6', false)
testDriver.test(() => { return m3[0] instanceof C }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_7', false)
testDriver.test(() => { return m3[0].re instanceof P }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_8', false)
testDriver.test(() => { return r.eq(m3[0].re, 1) }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_9', false)
testDriver.test(() => { return m3[0].im instanceof P }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_10', false)
testDriver.test(() => { return r.eq(m3[0].im, 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_11', false)
testDriver.test(() => { return m3[1] instanceof C }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_12', false)
testDriver.test(() => { return m3[1].re instanceof P }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_13', false)
testDriver.test(() => { return r.eq(m3[1].re, 2) }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_14', false)
testDriver.test(() => { return m3[1].im instanceof P }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_15', false)
testDriver.test(() => { return r.eq(m3[1].im, 0) }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_16', false)

let a = r.$(1, 2, 5)
testDriver.test(() => { return a instanceof Array }, true, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_17', false)
testDriver.test(() => { return l.cast(a) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_18', false)

testDriver.test(() => { return l.cast(2) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#cast-example0_19', false)
