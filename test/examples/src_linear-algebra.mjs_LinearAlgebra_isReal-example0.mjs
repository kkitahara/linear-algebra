import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let l = new LinearAlgebra(r)

testDriver.test(() => { return l.isReal() }, true, 'src/linear-algebra.mjs~LinearAlgebra#isReal-example0_0', false)
