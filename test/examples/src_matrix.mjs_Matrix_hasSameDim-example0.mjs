import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Matrix as M } from '../../src/index.mjs'

let m1 = new M(1, 2, 3, 4)
let m2 = new M(1, 2, 3, 4).setDim(1, 4)
let m3 = new M(1, 2, 3)
testDriver.test(() => { return m1.hasSameDim(m2) }, true, 'src/matrix.mjs~Matrix#hasSameDim-example0_0', false)

m2.setDim(4, 1)
testDriver.test(() => { return m1.hasSameDim(m2) }, true, 'src/matrix.mjs~Matrix#hasSameDim-example0_1', false)

m2.setDim(2, 2)
testDriver.test(() => { return m1.hasSameDim(m2) }, true, 'src/matrix.mjs~Matrix#hasSameDim-example0_2', false)

m1.setDim(2, 2)
testDriver.test(() => { return m1.hasSameDim(m2) }, true, 'src/matrix.mjs~Matrix#hasSameDim-example0_3', false)

m1.setDim(1, 4)
testDriver.test(() => { return m1.hasSameDim(m2) }, false, 'src/matrix.mjs~Matrix#hasSameDim-example0_4', false)

testDriver.test(() => { return m1.hasSameDim(m3) }, false, 'src/matrix.mjs~Matrix#hasSameDim-example0_5', false)

testDriver.test(() => { return m1.hasSameDim(null) }, Error, 'src/matrix.mjs~Matrix#hasSameDim-example0_6', false)
