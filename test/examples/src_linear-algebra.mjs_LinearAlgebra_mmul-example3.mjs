import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '@kkitahara/complex-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let c = new ComplexAlgebra(r)
let l = new LinearAlgebra(c)

testDriver.test(() => { return l.mmul(l.$(), l.$()).toString() }, '()', 'src/linear-algebra.mjs~LinearAlgebra#mmul-example3_0', false)
let m1 = l.$(1, 5, 3, 4).setDim(2, 0)
let m2 = l.$(1, 2, 3, 4).setDim(2, 0)
testDriver.test(() => { return l.mmul(l.lup(m1), m2) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example3_1', false)
testDriver.test(() => { return l.mmul(m1, l.lup(m2)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example3_2', false)
testDriver.test(() => { return l.mmul(l.$(1, 5).setDim(1, 2), l.$(1, 5).setDim(1, 2)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example3_3', false)
testDriver.test(() => { return l.mmul(l.$(1, 5), l.$(1, 5)) }, Error, 'src/linear-algebra.mjs~LinearAlgebra#mmul-example3_4', false)
