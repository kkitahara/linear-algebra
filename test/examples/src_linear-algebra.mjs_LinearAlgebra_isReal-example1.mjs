import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '@kkitahara/complex-algebra'
import { LinearAlgebra } from '../../src/index.mjs'
let r = new RealAlgebra()
let c = new ComplexAlgebra(r)
let l = new LinearAlgebra(c)

testDriver.test(() => { return l.isReal() }, false, 'src/linear-algebra.mjs~LinearAlgebra#isReal-example1_0', false)

testDriver.test(() => { return new LinearAlgebra(r).isReal() }, true, 'src/linear-algebra.mjs~LinearAlgebra#isReal-example1_1', false)
